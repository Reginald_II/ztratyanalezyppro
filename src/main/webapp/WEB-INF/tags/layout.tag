<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">
<link href="<c:url value="/resources/css/vlastniStyly.css"/>"
	type="text/css" rel="stylesheet" />
<link href="<c:url value="/lightbox2/dist/css/lightbox.min.css"/>"
	rel="stylesheet" />
</head>
<body>
	<header class="card-header hlavicka">
		<h1>
			<a href=<c:url value="/"/>>Ztráty a nálezy</a>
		</h1>
	</header>
	<nav>
		<ul class="nav">
			<li class="nav-item"><a href="<c:url value="/"/>"
				class="nav-link" title="Zobrazí index.">Index</a></li>
			<sec:authorize access="isAuthenticated()">
				<li class="nav-item"><a href="<c:url value="/vlastniVeci"/>"
					class="nav-link" title="Zobrazí vám vaše věci.">Vlastní věci</a></li>
			</sec:authorize>
			<sec:authorize access="isAuthenticated()">
				<li class="nav-item"><a href="<c:url value="/userInfo"/>"
					class="nav-link" title="Informace o vašem účtu a jeho nastavení.">Účet</a></li>
			</sec:authorize>
			<sec:authorize access="!isAuthenticated()">
				<li class="nav-item"><a href="<c:url value="/login"/>"
					class="nav-link" title="Umožní vám se přihlásit.">Přihlásit</a></li>
			</sec:authorize>
			<sec:authorize access="isAuthenticated()">
				<li class="nav-item"><a
					href="<c:url value="/reportedItemOwnershipIndex"/>"
					class="nav-link"
					title="Zobrazí hlášení se k nálezům a nalezené ztráty.">Přihlášky
						k nálezům a nalezené ztráty</a></li>
			</sec:authorize>
			<sec:authorize access="isAuthenticated()">
				<li class="nav-item"><a href="<c:url value="logout"/>"
					class="nav-link" title="Umožní vám se odhlásit.">Odhlásit</a></li>
			</sec:authorize>
		</ul>
	</nav>
	<main>
		<div class="container-fluid">
			<jsp:doBody />
		</div>
	</main>
	<footer class="card-footer paticka"> Vytvořeno v roce 2021,
		pro studijní účely. </footer>
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"
		integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
		crossorigin="anonymous"></script>
	<script src="<c:url value="/resources/js/PotvrzeniAkce.js"/>"></script>
</body>
</html>