<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Registrace</title>
</head>
<t:layout>
	<h2>Registrace</h2>
	<c:if test="${not empty error}">
		<div class="alert alert-danger alert-dismissible fade show"
			role="alert">
			<c:out value="${error}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<form:form method="POST" action="changeUserInfo" modelAttribute="user">
		<fieldset class="form-group">
			<form:label path="email" class="col-form-label">E-mail:</form:label>
			<form:input path="email" class="form-control" required="required"
				placeholder="E-mailová adresa" />
			<p>
				<form:errors path="email" />
			</p>
			<form:label path="name" class="col-form-label">Jméno:</form:label>
			<form:input path="name" class="form-control" required="required"
				placeholder="Jméno" />
			<p>
				<form:errors path="name" />
			</p>
			<form:label path="surname" class="col-form-label">Přijmení:</form:label>
			<form:input path="surname" class="form-control" required="required"
				placeholder="Přijmení" />
			<p>
				<form:errors path="surname" />
			</p>
			<form:label path="telephone" class="col-form-label">Telefonní číslo:</form:label>
			<form:input path="telephone" class="form-control" required="required"
				placeholder="Telefonní číslo" />
			<p>
				<form:errors path="telephone" />
			</p>
			<form:label path="streetName" class="col-form-label">Název ulice bydliště:</form:label>
			<form:input path="streetName" class="form-control"
				required="required" placeholder="Název ulice" />
			<p>
				<form:errors path="streetName" />
			</p>
			<form:label path="city.name" class="col-form-label">Název města bydliště:</form:label>
			<form:input path="city.name" class="form-control" required="required"
				placeholder="Název města" />
			<p>
				<form:errors path="city.name" />
			</p>
			<form:label path="city.psc" class="col-form-label">PSČ bydliště:</form:label>
			<form:input path="city.psc" class="form-control" required="required"
				placeholder="PSČ" />
			<p>
				<form:errors path="city.psc" />
			</p>
			<form:hidden path="id" />
			<form:hidden path="password" />
			<form:hidden path="login" />
		</fieldset>
		<fieldset class="form-group">
			<input type="submit" class="btn btn-primary" value="Změnit údaje" />
			<a href="<c:url value="/userInfo"/>" class="btn btn-secondary">Storno</a>
		</fieldset>
	</form:form>
</t:layout>