<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Změna hesla</title>
</head>
<t:layout>
	<h2>Změna hesla</h2>
	<c:if test="${not empty error}">
		<div class="alert alert-danger alert-dismissible fade show"
			role="alert">
			<c:out value="${error}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<form method="POST" action="<c:url value="/changePasswordProcess"/>">
		<fieldset class="form-group">
			<label for="oldPassword" class="col-form-label">Původní
				heslo:</label> <input type="password" name="oldPassword"
				class="form-control" required="required" placeholder="Původní heslo" />
			<label for="password" class="col-form-label">Nové heslo:</label> <input
				type="password" name="password" class="form-control"
				required="required" placeholder="Nové heslo" /> <label
				for="passwordAgain" class="col-form-label">Nové heslo znova:</label>
			<input type="password" name="passwordAgain" class="form-control"
				required="required" placeholder="Nové heslo znova" /> <input
				type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</fieldset>
		<fieldset class="form-group">
			<input type="submit" class="btn	btn-primary" value="Změnit heslo" />
			<a href="<c:url value="/userInfo"/>" class="btn btn-secondary">Storno</a>
		</fieldset>
	</form>
</t:layout>