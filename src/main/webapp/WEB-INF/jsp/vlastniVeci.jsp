<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Vlastní předměty</title>
</head>
<t:layout>
	<h2>Vlastní předměty</h2>
	<c:if test="${not empty success}">
		<div class="alert alert-success alert-dismissible fade show"
			role="alert">
			<c:out value="${success}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<c:if test="${not empty error}">
		<div class="alert alert-danger alert-dismissible fade show"
			role="alert">
			<c:out value="${error}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<h2>Seznam ztrát</h2>
	<form method="GET" action="<c:url value="/vlastniVeci"/>"
		class="form-inline mb-3">
		<fieldset class="form-group">
			<label for="name" class="col-form-label mr-1">Název předmětu:</label>
			<input type="text" name="name" class="form-control"
				placeholder="Název předmětu" /> <label for="lossStateId"
				class="col-form-label ml-1 mr-1">Druh: </label> <select
				name="lossStateId">
				<option value="0">Vše</option>
				<c:forEach items="${lossStates}" var="lossState">
					<option value="${lossState.id}"><c:out
							value="${lossState.description}" /></option>
				</c:forEach>
			</select>
		</fieldset>
		<fieldset class="form-group">
			<input type="submit" value="Hledat" class="ml-3" />
		</fieldset>
	</form>
	<a href="<c:url value="/pridatZtratu"/>" class="btn btn-link mb-3">Přidat
		ztrátu</a>
	<table class="table table-bordered">
		<thead class="thead-light">
			<tr>
				<th>Název</th>
				<th>Popis</th>
				<th>Druh</th>
				<th>Možnosti</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${ztr}" var="ztra">
				<tr>
					<td><a href="<c:url value="/detailedItemInfo?id=${ztra.id}"/>">${ztra.nazev}</a></td>
					<td><c:out value="${ztra.popis}" /></td>
					<td><c:out value="${ztra.lossState.description}" /></td>
					<td><c:if test="${ztra.lossState.id<3}">
							<a href="<c:url value="/upravitVlastniPredmet?id=${ztra.id}"/>">Upravit</a>
							<c:if test="${ztra.lossState.id<2}">
								<a href="<c:url value="/smazatVlastniZtratu?id=${ztra.id}"/>"
									class="smazat">Smazat</a>
							</c:if>
						</c:if></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<ul class="pagination">
		<c:forEach items="${pages}" var="page">
			<c:if test="${page==currentPage}">
				<li class="page-item active"><a
					href="<c:url value="/vlastniVeci?name=${name}&lossStateId=${lossStateId}&numberOfPage=${currentPage}"/>"
					class="page-link"><c:out value="${page}" /></a></li>
			</c:if>
			<c:if test="${page!=currentPage}">
				<li class="page-item"><a
					href="<c:url value="/vlastniVeci?name=${name}&lossStateId=${lossStateId}&numberOfPage=${page}"/>"
					class="page-link"><c:out value="${page}" /></a></li>
			</c:if>
		</c:forEach>
	</ul>
</t:layout>