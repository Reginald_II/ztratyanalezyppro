<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Přihlášky k vlastnictví předmětů a nalezené ztráty -
	rozcestí</title>
</head>
<t:layout>
	<h2>Přihlášky k vlastnictví předmětů a nalezené ztráty - rozcestí</h2>
	<ul>
		<li><a href="<c:url value="/reportedItemOwnership"/>">Přihlášky
				k vlastnictví předmětu</a></li>
		<li><a href="<c:url value="/foundLostItems"/>">Nalezené
				ztráty</a></li>
	</ul>
</t:layout>