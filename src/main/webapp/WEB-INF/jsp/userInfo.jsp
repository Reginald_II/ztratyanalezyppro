<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Účet</title>
</head>
<t:layout>
	<h2>Účet</h2>
	<c:if test="${not empty success}">
		<div class="alert alert-success alert-dismissible fade show"
			role="alert">
			<c:out value="${success}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<h3>Informace</h3>
	<p>
		Přihlašovací jméno:
		<c:out value="${user.login}" />
	</p>
	<p>
		Jméno:
		<c:out value="${user.name}" />
	</p>
	<p>
		Přijmení:
		<c:out value="${user.surname}" />
	</p>
	<p>
		Telefonní číslo:
		<c:out value="${user.telephone}" />
	</p>
	<p>
		Oprávnění:
		<c:out value="${user.rights.description}" />
	</p>
	<p>
		Ulice:
		<c:out value="${user.streetName}" />
	</p>
	<p>
		Město:
		<c:out value="${user.city.name}" />
	</p>
	<p>
		PSČ:
		<c:out value="${user.city.psc }" />
	</p>
	<h3>Možnosti</h3>
	<ul>
		<li><a href="<c:url value="/userSettings"/>">Změnit údaje</a></li>
		<li><a href="<c:url value="/changePassword"/>">Změnit heslo</a>
		<li><a href="<c:url value="/changeLogin"/>">Změnit
				přihlašovací jméno</a></li>
	</ul>
</t:layout>