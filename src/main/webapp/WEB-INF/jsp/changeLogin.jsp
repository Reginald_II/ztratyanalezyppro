<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Změna přihlašovacího jméno</title>
</head>
<t:layout>
	<h2>Změna přihlašovacího jméno</h2>
	<c:if test="${not empty error}">
		<div class="alert alert-danger alert-dismissible fade show"
			role="alert">
			<c:out value="${error}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<form method="POST" action="<c:url value="/changeLoginProcess"/>">
		<fieldset class="form-group">
			<label for="login" class="col-form-label">Nové přihlašovací
				jméno:</label> <input type="text" name="login" class="form-control"
				required="required" placeholder="Nové přihlašovací jméno" /> <input
				type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</fieldset>
		<fieldset class="form-group">
			<input type="submit" class="btn	btn-primary"
				value="Změnit přihlašovací jméno" /> <a
				href="<c:url value="/userInfo"/>" class="btn btn-secondary">Storno</a>
		</fieldset>
	</form>
</t:layout>