<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Seznam přihlášek k nálezu</title>
</head>
<t:layout>
	<h2>Seznam přihlášek k nálezu</h2>
	<c:if test="${not empty success}">
		<div class="alert alert-success alert-dismissible fade show"
			role="alert">
			<c:out value="${success}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<c:if test="${not empty error}">
		<div class="alert alert-danger alert-dismissible fade show"
			role="alert">
			<c:out value="${error}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<form method="GET" action="<c:url value="/reportedItemOwnership"/>"
		class="form-inline mb-3">
		<fieldset class="form-group">
			<label for="name" class="col-form-label mr-1">Název předmětu:</label>
			<input type="text" name="name" class="form-control"
				placeholder="Název předmětu" />
		</fieldset>
		<fieldset class="form-group">
			<input type="submit" value="Hledat" class="ml-3" />
		</fieldset>
	</form>
	<table class="table table-bordered">
		<thead class="thead-light">
			<tr>
				<th>Název</th>
				<th>Popis</th>
				<sec:authorize access="hasRole('ADMIN')">
					<th>Jméno hlásícího se vlastníka</th>
					<th>Přijmení hlásícího se vlastníka</th>
				</sec:authorize>
				<th>Možnosti</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${reports}" var="reportItem">
				<tr>
					<td><a
						href="<c:url value="/detailedItemInfo?id=${reportItem.ztrata.id}"/>"><c:out
								value="${reportItem.ztrata.nazev}" /></a></td>
					<td><c:out value="${reportItem.ztrata.popis}" /></td>
					<sec:authorize access="hasRole('ADMIN')">
						<td><c:out value="${reportItem.user.name}" /></td>
						<td><c:out value="${reportItem.user.surname}" /></td>
					</sec:authorize>
					<td><sec:authorize access="hasRole('ADMIN')">
							<a
								href="<c:url value="/giveItemToUser?id=${reportItem.id}&lostItem=false"/>"
								class="odevzdatPredmetUzivateli">Odevzdat uživateli</a>
							<c:if test="${userId!=reportItem.ztrata.user.id}">
								<a
									href="<c:url value="/odhlasitVlastnictviNalezu?id=${reportItem.ztrata.id}&detailed=false"/>">Odhlásit
									se od vlastnictví nálezu</a>
							</c:if>
						</sec:authorize> <sec:authorize access="hasRole('USER')">
							<a
								href="<c:url value="/odhlasitVlastnictviNalezu?id=${reportItem.ztrata.id}&detailed=false"/>">Odhlásit
								se od vlastnictví nálezu</a>
						</sec:authorize></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<ul class="pagination">
		<c:forEach items="${pages}" var="page">
			<c:if test="${page==currentPage}">
				<li class="page-item active"><a
					href="<c:url value="/reportedItemOwnership?name=${name}&numberOfPage=${currentPage}"/>"
					class="page-link"><c:out value="${page}" /></a></li>
			</c:if>
			<c:if test="${page!=currentPage}">
				<li class="page-item"><a
					href="<c:url value="/reportedItemOwnership?name=${name}&numberOfPage=${page}"/>"
					class="page-link"><c:out value="${page}" /></a></li>
			</c:if>
		</c:forEach>
	</ul>
</t:layout>