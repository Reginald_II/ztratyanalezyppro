<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Přidat ztrátu</title>
</head>
<t:layout>
	<h2>Přidat ztrátu</h2>
	<c:if test="${not empty error}">
		<div class="alert alert-danger alert-dismissible fade show"
			role="alert">
			<c:out value="${error}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<form:form method="POST" action="pridaniZtraty" modelAttribute="ztrata"
		enctype="multipart/form-data">
		<fieldset class="form-group">
			<form:label path="nazev" class="col-form-label">Název:</form:label>
			<form:input path="nazev" class="form-control" required="required"
				placeholder="Název ztráty" />
			<p>
				<form:errors path="nazev" />
			</p>
			<form:label path="popis" class="col-form-label">Popis:</form:label>
			<form:textarea path="popis" class="form-control" required="required"
				placeholder="Popis ztráty" />
			<p>
				<form:errors path="popis" />
			</p>
			<label for="pictureFile" class="col-form-label">Fotka ztráty
				(maximálně 16 MB, pouze ve formátu .jpg nebo .png):</label> <input
				type="file" name="pictureFile" accept=".jpg, .png" />
		</fieldset>
		<fieldset class="form-group">
			<input type="submit" class="btn btn-primary" value="Přidat" /> <a
				href="<c:url value="/vlastniVeci"/>" class="btn btn-secondary">Storno</a>
		</fieldset>
	</form:form>
</t:layout>