<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Index</title>
</head>
<t:layout>
	<h2>Index</h2>
	<c:if test="${not empty success}">
		<div class="alert alert-success alert-dismissible fade show"
			role="alert">
			<c:out value="${success}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<c:if test="${not empty error}">
		<div class="alert alert-danger alert-dismissible fade show"
			role="alert">
			<c:out value="${error}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<form method="GET" action="<c:url value="/"/>" class="form-inline mb-3">
		<fieldset class="form-group">
			<label for="name" class="col-form-label mr-1">Název předmětu:</label>
			<input type="text" name="name" class="form-control"
				placeholder="Název předmětu" />
			<sec:authorize access="hasRole('ADMIN')">
				<label for="lossStateId" class="col-form-label ml-1 mr-1">Druh:
				</label>
				<select name="lossStateId">
					<option value="0">Vše</option>
					<c:forEach items="${lossStates}" var="lossState">
						<option value="${lossState.id}"><c:out
								value="${lossState.description}" /></option>
					</c:forEach>
				</select>
			</sec:authorize>
		</fieldset>
		<fieldset class="form-group">
			<input type="submit" value="Hledat" class="ml-3" />
		</fieldset>
	</form>
	<sec:authorize access="hasRole('ADMIN')">
		<h3>Seznam předmětů</h3>
	</sec:authorize>
	<sec:authorize access="hasRole('USER')">
		<h3>Seznam nálezů</h3>
	</sec:authorize>
	<sec:authorize access="hasRole('ADMIN')">
		<a href="<c:url value="/pridatNalez"/>" class="btn btn-link mb-3">Přidat
			nález</a>
	</sec:authorize>
	<table class="table table-bordered">
		<thead class="thead-light">
			<tr>
				<th>Název</th>
				<th>Popis</th>
				<th>Druh</th>
				<sec:authorize access="hasRole('ADMIN')">
					<th>Jméno autora</th>
					<th>Přijmení autora</th>
					<th>Možnosti</th>
				</sec:authorize>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${ztr}" var="ztra">
				<tr>
					<td><a href="<c:url value="/detailedItemInfo?id=${ztra.id}"/>"><c:out
								value="${ztra.nazev}" /></a></td>
					<td><c:out value="${ztra.popis}" /></td>
					<td><c:out value="${ztra.lossState.description}" /></td>
					<sec:authorize access="hasRole('ADMIN')">
						<sec:authorize access="hasRole('ADMIN')">
							<td><c:out value="${ztra.user.name}" /></td>
							<td><c:out value="${ztra.user.surname}" /></td>
						</sec:authorize>
						<td><c:if test="${ztra.lossState.id!=3}">
								<a href="<c:url value="/upravitPredmet?id=${ztra.id}"/>">Upravit</a>
							</c:if> <c:if test="${ztra.lossState.id<2}">
								<a href="<c:url value="/smazatZtratu?id=${ztra.id}"/>"
									class="smazat">Smazat</a>
							</c:if> <c:if test="${ztra.lossState.id==1}">
								<a href="<c:url value="/prevestZtratuNaNalez?id=${ztra.id}"/>"
									class="prevestNaNalez">Převést ztrátu na nález</a>
							</c:if></td>
					</sec:authorize>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<ul class="pagination">
		<c:forEach items="${pages}" var="page">
			<c:if test="${page==currentPage}">
				<li class="page-item active"><a
					href="<c:url value="/?name=${name}&lossStateId=${lossStateId}&numberOfPage=${currentPage}"/>"
					class="page-link"><c:out value="${page}" /></a></li>
			</c:if>
			<c:if test="${page!=currentPage}">
				<li class="page-item"><a
					href="<c:url value="/?name=${name}&lossStateId=${lossStateId}&numberOfPage=${page}"/>"
					class="page-link"><c:out value="${page}" /></a></li>
			</c:if>
		</c:forEach>
	</ul>
</t:layout>