<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Přihlášení</title>
</head>
<t:layout>
	<h2>Přihlášení</h2>
	<c:if test="${not empty success}">
		<div class="alert alert-success alert-dismissible fade show"
			role="alert">
			<c:out value="${success}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<c:if test="${not empty error}">
		<div class="alert alert-danger alert-dismissible fade show"
			role="alert">
			<c:out value="${error}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<form:form method="POST" action="doLogin" modelAttribute="user">
		<fieldset class="form-group">
			<form:label path="login" class="col-form-label">Přihlašovací jméno:</form:label>
			<form:input path="login" class="form-control" required="required"
				placeholder="Přihlašovací jméno" />
			<p>
				<form:errors path="login" />
			</p>
			<form:label path="password" class="col-form-label">Heslo:</form:label>
			<form:password path="password" class="form-control"
				required="required" placeholder="Heslo" />
			<p>
				<form:errors path="password" />
			</p>
		</fieldset>
		<fieldset class="form-group">
			<input type="submit" class="btn	btn-primary" value="Přihlásit se" />
		</fieldset>
	</form:form>
	<p>
		<a href="<c:url value="/register"/>">Registrace</a>
	</p>
</t:layout>