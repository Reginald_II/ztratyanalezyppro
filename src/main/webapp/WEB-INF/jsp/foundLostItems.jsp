<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Nalezené ztráty</title>
</head>
<t:layout>
	<h2>Nalezené ztráty</h2>
	<c:if test="${not empty success}">
		<div class="alert alert-success alert-dismissible fade show"
			role="alert">
			<c:out value="${success}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<c:if test="${not empty error}">
		<div class="alert alert-danger alert-dismissible fade show"
			role="alert">
			<c:out value="${error}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<form method="GET" action="<c:url value="/foundLostItems"/>"
		class="form-inline mb-3">
		<fieldset class="form-group">
			<label for="name" class="col-form-label mr-1">Název předmětu:</label>
			<input type="text" name="name" class="form-control"
				placeholder="Název předmětu" />
		</fieldset>
		<fieldset class="form-group">
			<input type="submit" value="Hledat" class="ml-3" />
		</fieldset>
	</form>
	<table class="table table-bordered">
		<thead class="thead-light">
			<tr>
				<th>Název</th>
				<th>Popis</th>
				<sec:authorize access="hasRole('ADMIN')">
					<th>Jméno vlastníka</th>
					<th>Přijmení hlásícího se vlastníka</th>
				</sec:authorize>
				<sec:authorize access="hasRole('ADMIN')">
					<th>Možnosti</th>
				</sec:authorize>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${foundLosses}" var="foundLoss">
				<tr>
					<td><a
						href="<c:url value="/detailedItemInfo?id=${foundLoss.id}"/>"><c:out
								value="${foundLoss.nazev}" /></a></td>
					<td><c:out value="${foundLoss.popis}" /></td>
					<sec:authorize access="hasRole('ADMIN')">
						<td><c:out value="${foundLoss.user.name}" /></td>
						<td><c:out value="${foundLoss.user.surname}" /></td>
					</sec:authorize>
					<sec:authorize access="hasRole('ADMIN')">
						<td><a
							href="<c:url value="/giveItemToUser?id=${foundLoss.id}&lostItem=true"/>"
							class="odevzdatPredmetUzivateli">Odevzdat uživateli</a></td>
					</sec:authorize>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<ul class="pagination">
		<c:forEach items="${pages}" var="page">
			<c:if test="${page==currentPage}">
				<li class="page-item active"><a
					href="<c:url value="/foundLostItems?name=${name}&numberOfPage=${currentPage}"/>"
					class="page-link"><c:out value="${page}" /></a></li>
			</c:if>
			<c:if test="${page!=currentPage}">
				<li class="page-item"><a
					href="<c:url value="/foundLostItems?name=${name}&numberOfPage=${page}"/>"
					class="page-link"><c:out value="${page}" /></a></li>
			</c:if>
		</c:forEach>
	</ul>
</t:layout>