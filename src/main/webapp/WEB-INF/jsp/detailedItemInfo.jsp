<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@page isELIgnored="false"%>
<head>
<title>Podrobné zobrazení</title>
</head>
<t:layout>
	<h2>Podrobné zobrazení</h2>
	<c:if test="${not empty success}">
		<div class="alert alert-success alert-dismissible fade show"
			role="alert">
			<c:out value="${success}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<c:if test="${not empty error}">
		<div class="alert alert-danger alert-dismissible fade show"
			role="alert">
			<c:out value="${error}" />
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">x</button>
		</div>
	</c:if>
	<div class="card itemDescription mb-3">
		<div class="card-header">
			<h3>Předmět</h3>
		</div>
		<div class="card-body">
			<p>
				Název:
				<c:out value="${ztrata.nazev}" />
			</p>
			<p>
				Popis:
				<c:out value="${ztrata.popis}" />
			</p>
			<p>
				Typ:
				<c:out value="${ztrata.lossState.description}" />
			</p>
			<p>
				Datum vytvoření:
				<fmt:formatDate value="${ztrata.timeCreated}"
					pattern="dd.MM.yyyy HH:mm:ss" />
			</p>
			<p>
				Datum poslední změny:
				<fmt:formatDate value="${ztrata.timeChanged}"
					pattern="dd.MM.yyyy HH:mm:ss" />
			</p>
			<p>
				Jméno vkladatele:
				<c:out value="${ztrata.user.name}" />
			</p>
			<p>
				Přijmení vkladatele:
				<c:out value="${ztrata.user.surname}" />
			</p>
			<c:if test="${not empty ztrata.itemReturned}">
				<p>
					Jméno uživatele, kterému se předmět vrátil:
					<c:out value="${ztrata.itemReturned.user.name}" />
				</p>
				<p>
					Příjmení uživatele, kterému se předmět vrátil:
					<c:out value="${ztrata.itemReturned.user.surname}" />
				</p>
				<p>
					Datum vrácení:
					<c:out value="${dateOfReturn}" />
				</p>
			</c:if>
		</div>
	</div>
	<c:if test="${not empty ztrata.picture}">
		<div class="card ml-5 pictureOfItem">
			<div class="card-body">
				<a href="<c:url value="/getPicture?id=${ztrata.picture.id}"/>"
					data-lightbox="${ztrata.nazev}" data-title="${ztrata.nazev}"><img
					src="<c:url value="/getPicture?id=${ztrata.picture.id}"/>"
					alt="${ztrata.nazev}" title="${ztrata.nazev}" width="100"
					height="100" /></a>
			</div>
		</div>
	</c:if>
	<sec:authorize access="hasRole('ADMIN')">
		<c:if test="${not empty reportingUsers}">
			<h3>Žádosti vlastníka o návrat předmětu</h3>
			<table class="table table-bordered">
				<thead class="thead-light">
					<tr>
						<th>Jméno</th>
						<th>Přijmení</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${reportingUsers}" var="reportingUser">
						<tr>
							<td><c:out value="${reportingUser.user.name}" /></td>
							<td><c:out value="${reportingUser.user.surname}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
	</sec:authorize>
	<c:if test="${!ownItem}">
		<c:if test="${ztrata.lossState.id==2&&!ztrata.wasCreatedAsLoss}">
			<h3>Možnosti</h3>
			<c:if test="${!reported}">
				<p>
					<a href="<c:url value="/hlasitVlastnictviNalezu?id=${ztrata.id}"/>">Přihlásit
						se k vlastnictví nálezu</a>
				</p>
			</c:if>
			<c:if test="${reported}">
				<p>
					<a
						href="<c:url value="/odhlasitVlastnictviNalezu?id=${ztrata.id}&detailed=true"/>">Odhlásit
						se od vlastnictví nálezu</a>
				</p>
			</c:if>
		</c:if>
	</c:if>
</t:layout>
<script src="<c:url value="/lightbox2/dist/js/lightbox.min.js"/>"></script>