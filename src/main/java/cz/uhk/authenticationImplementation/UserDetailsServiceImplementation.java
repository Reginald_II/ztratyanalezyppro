package cz.uhk.authenticationImplementation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import cz.uhk.dao.UserDao;
import cz.uhk.model.User;

@Service("userDetailsService")
public class UserDetailsServiceImplementation implements UserDetailsService {
	private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImplementation.class);

	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(username);

		if (user == null) {
			throw new UsernameNotFoundException("User wasn´t found by the inserted login!");
		}
		log.info("loadUserByUsername() : {}", username);
		return new UserDetailsImplementation(user);
	}

}
