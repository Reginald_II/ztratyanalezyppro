package cz.uhk.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import cz.uhk.authenticationImplementation.UserDetailsServiceImplementation;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	@Bean
	public UserDetailsService userDetailsService() {
		return new UserDetailsServiceImplementation();
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider daoAuthenticationProvider() {
		final DaoAuthenticationProvider daoAuthProvider = new DaoAuthenticationProvider();
		daoAuthProvider.setUserDetailsService(userDetailsService());
		daoAuthProvider.setPasswordEncoder(passwordEncoder());
		return daoAuthProvider;
	}

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) {

		auth.authenticationProvider(daoAuthenticationProvider());
	}

	@Override
	protected void configure(final HttpSecurity http) {
		try {
			http.authorizeRequests().antMatchers("/user").authenticated().and().authorizeRequests()
					.antMatchers("/changeLogin").authenticated().and().authorizeRequests()
					.antMatchers("/changePassword").authenticated().and().authorizeRequests()
					.antMatchers("/changeUserInfo").authenticated().and().formLogin().loginPage("/login")
					.usernameParameter("login").passwordParameter("password").permitAll().loginProcessingUrl("/doLogin")
					.successForwardUrl("/postLogin").failureUrl("/loginFailed").and().logout().logoutUrl("/logout")
					.logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET")).logoutSuccessUrl("/login")
					.permitAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
