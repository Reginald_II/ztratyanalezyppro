package cz.uhk.utility;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 * Utility class for generating Hibernate´s API instances.
 * 
 * @author Artur Hamza
 *
 */
public abstract class HibernateUtility {
	/**
	 * Creates new instance of SessionFactory.
	 * 
	 * @return
	 */
	public static SessionFactory createSessionFactory() {
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
		final MetadataSources sources = new MetadataSources(registry);
		final Metadata metadata = sources.getMetadataBuilder().build();
		final SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
		return sessionFactory;
	}
}
