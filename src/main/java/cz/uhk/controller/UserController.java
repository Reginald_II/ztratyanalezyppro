package cz.uhk.controller;

import javax.validation.Valid;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.dao.CityDao;
import cz.uhk.dao.UserDao;
import cz.uhk.model.City;
import cz.uhk.model.User;

/**
 * Manages user actions.
 * 
 * @author Artur Hamza
 *
 */
@Controller
@Secured({ "ROLE_ADMIN", "ROLE_USER" })
public class UserController {
	@GetMapping("/userInfo")
	public ModelAndView index() {
		final ModelAndView model = new ModelAndView("userInfo");
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		model.addObject(user);
		return model;
	}

	/**
	 * Gets required data for the user´s settings and returns the view, where user
	 * can change all his information, aside of password and login.
	 * 
	 * @return view with form for changing the user´s information.
	 */
	@GetMapping("/userSettings")
	public ModelAndView userSettings() {
		final ModelAndView model = new ModelAndView("userSettings");
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		model.addObject(user);
		return model;
	}

	/**
	 * Processess the change of the information of an user.
	 * 
	 * @param user     user information from the form.
	 * @param result   result of the user´s validation.
	 * @param redirect instance to save data for redirected action.
	 * @return result of the action.
	 */
	@RequestMapping(path = "/changeUserInfo", method = RequestMethod.POST)
	public ModelAndView changeUserInfo(@Valid final User user, final BindingResult result,
			final RedirectAttributes redirect) {
		final ModelAndView model;
		if (result.hasErrors()) {
			model = new ModelAndView("userSettings");
			model.addObject(user);
			return model;
		}
		final UserDao userDao = new UserDao();
		final User validUser = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		// Checks if user isn´t trying to change other user´s account.
		if (user.getId() != validUser.getId()) {
			model = new ModelAndView("redirect:/");
			redirect.addFlashAttribute("error", "Snažíte se upravit cizí účet!");
			return model;
		}
		final CityDao cityDao = new CityDao();
		// Checks if the city already exists by its PSC, if not, the city is created.
		if (cityDao.getByPSC(user.getCity().getPsc()) != null) {
			user.setCity(cityDao.getByPSC(user.getCity().getPsc()));
		} else {
			final City city = user.getCity();
			cityDao.create(city);
		}
		user.setRights(validUser.getRights());
		user.setLogin(validUser.getLogin());
		user.setPassword(validUser.getPassword());
		userDao.merge(user);
		model = new ModelAndView("redirect:/userInfo");
		redirect.addFlashAttribute("success", "Údaje vašeho účtu byly úspěšně změněny.");
		return model;
	}

	/**
	 * Returns view with form to change a password.
	 * 
	 * @return view with form to change a password.
	 */
	@GetMapping("/changePassword")
	public String changePassword() {
		return "changePassword";
	}

	/**
	 * Processess the change of a password of an user.
	 * 
	 * @param oldPassword   old password.
	 * @param password      new password wrote for the first time.
	 * @param passwordAgain new password wrote for the second time.
	 * @return result of the action.
	 */
	@RequestMapping(path = "/changePasswordProcess", method = RequestMethod.POST)
	public ModelAndView changeUserPasswordProcess(@RequestParam final String oldPassword,
			@RequestParam final String password, @RequestParam final String passwordAgain) {
		final ModelAndView model;
		if (oldPassword.isBlank() || password.isBlank() || passwordAgain.isBlank()) {
			model = new ModelAndView("changePassword");
			model.addObject("error", "Položky Staré heslo, Nové heslo a Nové heslo znova nesmí být prázdné!");
			return model;
		}
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		// Checks if user did write his old password right.
		if (!encoder.matches(oldPassword, user.getPassword())) {
			model = new ModelAndView("changePassword");
			model.addObject("error", "Zadal jste špatně staré heslo. Zadejte ho prosím znova a správně.");
			return model;
		}
		// Checks if user wrote the new password twice right.
		if (!password.equals(passwordAgain)) {
			model = new ModelAndView("changePassword");
			model.addObject("error", "Položky Nové heslo a Nové heslo znova musí být stejné!");
			return model;
		}

		user.setPassword(encoder.encode(password));
		userDao.update(user);
		model = new ModelAndView("userInfo");
		model.addObject(user);
		model.addObject("success", "Vaše heslo bylo uspěšně změněno.");
		return model;
	}

	/**
	 * Returns a view with form for changing a user´s login.
	 * 
	 * @return view with form for changing a user´s login.
	 */
	@GetMapping("/changeLogin")
	public String changeLogin() {
		return "changeLogin";
	}

	/**
	 * Processes the change of the user´s login.
	 * 
	 * @param login login of a user.
	 * @return result of the action.
	 */
	@RequestMapping(path = "/changeLoginProcess", method = RequestMethod.POST)
	public ModelAndView changeLoginProcess(@RequestParam final String login) {
		final ModelAndView model;
		if (login.isBlank()) {
			model = new ModelAndView("changeLogin");
			model.addObject("error", "Přihlašovací jméno je prázdné! Prosím vyplňte ho!");
			return model;
		}
		final UserDao userDao = new UserDao();
		// Checks if the new login is already used by other user.
		if (userDao.getByLogin(login) != null) {
			model = new ModelAndView("changeLogin");
			model.addObject("error", "Přihlašovací jméno je obsazené! Zadejte prosím jiné!");
			return model;
		}
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		user.setLogin(login);
		userDao.update(user);
		model = new ModelAndView("userInfo");
		model.addObject(user);
		model.addObject("success", "Vaše přihlašovací jméno bylo uspěšně změněno.");
		return model;
	}
}
