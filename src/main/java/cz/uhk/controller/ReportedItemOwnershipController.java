package cz.uhk.controller;

import java.util.Date;
import java.util.List;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.dao.ItemReturnedDao;
import cz.uhk.dao.LossStateDao;
import cz.uhk.dao.ReportingUserDao;
import cz.uhk.dao.UserDao;
import cz.uhk.dao.ZtrataDao;
import cz.uhk.model.ItemReturned;
import cz.uhk.model.LossState;
import cz.uhk.model.ReportingUser;
import cz.uhk.model.User;
import cz.uhk.model.Ztrata;

/**
 * Manages actions connected with users reporting as owners of found items.
 * 
 * @author Artur Hamza
 *
 */
@Controller
@Secured({ "ROLE_ADMIN", "ROLE_USER" })
public class ReportedItemOwnershipController {
	/**
	 * Returns an index of the controller.
	 * 
	 * @return index of the controller.
	 */
	@GetMapping(path = "/reportedItemOwnershipIndex")
	public String index() {
		return "reportedItemOwnershipIndex";
	}

	/**
	 * Manages getting data and returning view showing table of users reporting
	 * themselves as owners of found items. The table can be limited by name of an
	 * item.
	 * 
	 * @param name         name of items.
	 * @param numberOfPage number of actual page.
	 * @return view showing table of reported item ownership.
	 */
	@GetMapping(path = "/reportedItemOwnership")
	public ModelAndView reportedItemOwnership(@RequestParam(required = false) final String name,
			@RequestParam(defaultValue = "0") int numberOfPage) {
		// Calculates the first item.
		final int firstItem;
		if (numberOfPage < 2) {
			numberOfPage = 1;
			firstItem = 0;
		} else {
			firstItem = 5 * (numberOfPage - 1);
		}
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		List<ReportingUser> reportingUsers;
		final ReportingUserDao reportingUserDao = new ReportingUserDao();
		final ModelAndView model = new ModelAndView("reportedItemOwnership");
		// If user is admin, he will see all reported owners.
		if (user.getRights().getName().equals("ROLE_ADMIN")) {
			if (name != null && !name.isBlank()) {
				reportingUsers = reportingUserDao.getAllFoundItemsOfReportedUsersByName(name);
				// If user isn´t admin, he will see only his own reports.
			} else {
				reportingUsers = reportingUserDao.getAllFoundItemsOfReportedUsers();
			}
			model.addObject("userId", user.getId());
		} else {
			if (name != null && !name.isBlank()) {
				reportingUsers = reportingUserDao.getFoundItemsOfReportedUserByName(name, user.getId());
			} else {
				reportingUsers = reportingUserDao.getFoundItemsOfReportedUser(user.getId());
			}
		}
		// Calculates number of pages.
		final int numberOfPages = (int) ((double) reportingUsers.size() / 5 + 0.9);
		final int[] pages;
		if (numberOfPages > 0) {
			pages = new int[numberOfPages];
		} else {
			pages = new int[1];
		}
		// Fills pages for later use by the for each tag of jsp.
		for (int i = 0; i < pages.length; i++) {
			pages[i] = i + 1;
		}
		// Changes the list to new one, containing only 5 instances, or if the number of
		// next five items after the first one would be lesser than 5, the lesser amount
		// of instances.
		if (reportingUsers.size() < firstItem + 5) {
			reportingUsers = reportingUsers.subList(firstItem, reportingUsers.size());
		} else {
			reportingUsers = reportingUsers.subList(firstItem, firstItem + 5);
		}
		model.addObject("reports", reportingUsers);
		model.addObject("pages", pages);
		model.addObject("currentPage", numberOfPage);
		if (name != null && !name.isBlank()) {
			model.addObject("name", name);
		}
		return model;
	}

	/**
	 * Gets found items, which were created as lossess and shows them in view.
	 * 
	 * @param name         name of items
	 * @param numberOfPage number of actual page
	 * @return view with table of found items.
	 */
	@GetMapping(path = "/foundLostItems")
	public ModelAndView foundLostItems(@RequestParam(required = false) final String name,
			@RequestParam(defaultValue = "0") int numberOfPage) {
		final int firstItem;
		if (numberOfPage < 2) {
			numberOfPage = 1;
			firstItem = 0;
		} else {
			firstItem = 5 * (numberOfPage - 1);
		}
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		final ModelAndView model = new ModelAndView("foundLostItems");
		final ZtrataDao ztrataDao = new ZtrataDao();
		List<Ztrata> foundItems;
		if (user.getRights().getName().equals("ROLE_ADMIN")) {
			if (name != null && !name.isBlank()) {
				foundItems = ztrataDao.getFoundLossessByName(name);
			} else {
				foundItems = ztrataDao.getFoundLossess();
			}
		} else {
			if (name != null && !name.isBlank()) {
				foundItems = ztrataDao.getFoundLossessOfUserByName(name, user.getId());
			} else {
				foundItems = ztrataDao.getFoundLossessOfUser(user.getId());
			}
		}
		final int numberOfPages = (int) ((double) foundItems.size() / 5 + 0.9);
		final int[] pages;
		if (numberOfPages > 0) {
			pages = new int[numberOfPages];
		} else {
			pages = new int[1];
		}
		for (int i = 0; i < pages.length; i++) {
			pages[i] = i + 1;
		}
		if (foundItems.size() < firstItem + 5) {
			foundItems = foundItems.subList(firstItem, foundItems.size());
		} else {
			foundItems = foundItems.subList(firstItem, firstItem + 5);
		}
		model.addObject("foundLosses", foundItems);
		model.addObject("pages", pages);
		model.addObject("currentPage", numberOfPage);
		if (name != null && !name.isBlank()) {
			model.addObject("name", name);
		}
		return model;
	}

	/**
	 * Processess the reporting of a user to ownership of a found item.
	 * 
	 * @param id          id of an item.
	 * @param redirectatt instance to save data for a redirected action.
	 * @return result of the action.
	 */
	@GetMapping(path = "/hlasitVlastnictviNalezu")
	public String reportOfOwnership(@RequestParam final int id, final RedirectAttributes redirectatt) {
		final ZtrataDao ztrataDao = new ZtrataDao();
		final Ztrata ztrata = ztrataDao.getById(id, Ztrata.class);
		// Checks if the item is found.
		if (ztrata.getLossState().getId() != 2) {
			redirectatt.addFlashAttribute("error", "Toto není nález!");
			return "redirect:/";
		}
		// Check if the item was created as loss, as found losses already have an owner
		// - the user, who created them.
		if (ztrata.isWasCreatedAsLoss()) {
			redirectatt.addFlashAttribute("error", "Nemůžete se hlásit o vlastnictví nalezené ztráty!");
			return "redirect:/";
		}
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		final ReportingUserDao reportingUserDao = new ReportingUserDao();
		// Checks if user is already reported to the item.
		if (reportingUserDao.getByItemForUser(id, user.getId()) != null) {
			redirectatt.addFlashAttribute("error", "K tomuto nálezu už jste přihlášen!");
			return "redirect:/detailedItemInfo?id=" + id;
		}
		final ReportingUser reportingUser = new ReportingUser(new Date(), user, ztrata);
		reportingUserDao.create(reportingUser);
		redirectatt.addFlashAttribute("success",
				"Přihlášení vlastnictví k nálezu " + ztrata.getNazev() + " bylo úspěšně provedeno.");
		return "redirect:/detailedItemInfo?id=" + id;
	}

	/**
	 * Processess removing of user´s report to an owneship of an item.
	 * 
	 * @param id          id of a found item
	 * @param detailed    information if the user came from the detailed item info
	 *                    website or not.
	 * @param redirectatt instance for saving data for redirected action.
	 * @return result of the action.
	 */
	@GetMapping(path = "/odhlasitVlastnictviNalezu")
	public String deleteReportOfOwnership(@RequestParam final int id,
			@RequestParam(defaultValue = "false") final boolean detailed, final RedirectAttributes redirectatt) {
		final ZtrataDao ztrataDao = new ZtrataDao();
		final Ztrata ztrata = ztrataDao.getById(id, Ztrata.class);
		// Checks if the item has other loss state category than found one.
		if (ztrata.getLossState().getId() != 2) {
			redirectatt.addFlashAttribute("error", "Toto není nález!");
			return "redirect:/reportedItemOwnership";
		}
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		final ReportingUserDao reportingUserDao = new ReportingUserDao();
		final ReportingUser reportingUser = reportingUserDao.getByItemForUser(ztrata.getId(), user.getId());
		// Checks if the user has already reported to the found item.
		if (reportingUser == null) {
			redirectatt.addFlashAttribute("error", "K tomuhle nálezu nemáte přihlášené vlastnictví!");
			if (detailed) {
				return "redirect:/detailedItemInfo?id=" + id;
			} else {
				return "redirect:/reportedItemOwnership";
			}
		}
		reportingUserDao.delete(reportingUser);
		redirectatt.addFlashAttribute("success",
				"Odhlášení vlastnictví nálezu " + ztrata.getNazev() + " bylo úspěšně provedeno.");
		// If user came from the detailed item info, he will be returned there.
		// Otherwise, he will be returned back to reported item ownership action.
		if (detailed) {
			return "redirect:/detailedItemInfo?id=" + id;
		} else {
			return "redirect:/reportedItemOwnership";
		}
	}

	/**
	 * Processes giving a found item to a user.
	 * 
	 * @param id          id of an either found item, which was created as loss, or
	 *                    of an instance of ReportingUser class.
	 * @param lostItem    information if the item to be returned it found item which
	 *                    created as loss, or if it´s based on ReportingUser
	 *                    instance.
	 * @param redirectatt instance for saving data for redirected action.
	 * @return result of the action.
	 */
	@GetMapping(path = "/giveItemToUser")
	@Secured("ROLE_ADMIN")
	public String giveItemToUser(@RequestParam final int id, @RequestParam final boolean lostItem,
			final RedirectAttributes redirectatt) {
		final ZtrataDao ztrataDao = new ZtrataDao();
		final LossStateDao lossStateDao = new LossStateDao();
		final ItemReturnedDao itemReturnedDao = new ItemReturnedDao();
		// If the boolean is true, then the action is going to work with found item,
		// which was created as lost one.
		if (lostItem) {
			final Ztrata ztrata = ztrataDao.getById(id, Ztrata.class);
			final ItemReturned itemReturned = new ItemReturned(new Date(), ztrata.getUser());
			itemReturnedDao.create(itemReturned);
			ztrata.setItemReturned(itemReturned);
			ztrata.setLossState(lossStateDao.getById(3, LossState.class));
			ztrataDao.update(ztrata);
			redirectatt.addFlashAttribute("success", "Předmět " + ztrata.getNazev() + " byl úspěšně přidělen uživateli "
					+ ztrata.getUser().getName() + " " + ztrata.getUser().getSurname() + ".");
			return "redirect:/foundLostItems";
		}
		// In case of boolean lostItem being false, the action is going to work with a
		// ReportingUser instance.
		final ReportingUserDao reportingUserDao = new ReportingUserDao();
		final ReportingUser reportingUser = reportingUserDao.getById(id, ReportingUser.class);
		final ItemReturned itemReturned = new ItemReturned(new Date(), reportingUser.getUser());
		itemReturnedDao.create(itemReturned);
		final Ztrata ztrata = reportingUser.getZtrata();
		ztrata.setItemReturned(itemReturned);
		ztrata.setLossState(lossStateDao.getById(3, LossState.class));
		ztrataDao.update(ztrata);
		redirectatt.addFlashAttribute("success", "Předmět " + ztrata.getNazev() + " byl úspěšně přidělen uživateli "
				+ reportingUser.getUser().getName() + " " + reportingUser.getUser().getSurname() + ".");
		return "redirect:/reportedItemOwnership";
	}
}
