package cz.uhk.controller;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import cz.uhk.dao.LossStateDao;
import cz.uhk.dao.ReportingUserDao;
import cz.uhk.dao.UserDao;
import cz.uhk.dao.ZtrataDao;
import cz.uhk.model.LossState;
import cz.uhk.model.ReportingUser;
import cz.uhk.model.User;
import cz.uhk.model.Ztrata;

/**
 * Controller of an index part of the application.
 * 
 * @author Artur Hamza
 *
 */
@Controller
@Secured({ "ROLE_ADMIN", "ROLE_USER" })
public class IndexController {
	/**
	 * Gets items for a view to show. Either all items for an admin or only found
	 * items for a user. Also items can be limited by name, their loss state
	 * category (admin only), and of course by paging. Only 5 items is shown per
	 * page.
	 * 
	 * @param name         name of items.
	 * @param lossStateId  id of a loss state category.
	 * @param numberOfPage number of current page.
	 * @return view with table of either all items for admin or found items for
	 *         other users.
	 */
	@GetMapping(path = "/")
	public ModelAndView index(@RequestParam(required = false) final String name,
			@RequestParam(defaultValue = "0") final int lossStateId,
			@RequestParam(defaultValue = "0") int numberOfPage) {
		final int firstItem;
		// Calculates first item depending on the number of current page.
		if (numberOfPage < 2) {
			numberOfPage = 1;
			firstItem = 0;
		} else {
			firstItem = 5 * (numberOfPage - 1);
		}

		final ModelAndView model = new ModelAndView("index");
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		final ZtrataDao ztrataDao = new ZtrataDao();
		List<Ztrata> ztraty;
		// Gets items for admin.
		if (user != null && user.getRights().getName().equals("ROLE_ADMIN")) {
			if ((name != null && !name.isBlank()) && (lossStateId > 0 && lossStateId <= 3)) {
				ztraty = ztrataDao.getItemsByCategoryAndName(name, lossStateId);
			} else if (name != null && !name.isBlank()) {
				ztraty = ztrataDao.getByName(name);
			} else if (lossStateId > 0 && lossStateId <= 3) {
				ztraty = ztrataDao.getItemsByCategory(lossStateId);
			} else {
				ztraty = ztrataDao.getAll(Ztrata.class);
			}
			// Gets items for a user or other user categories.
		} else {
			if (name == null || name.isBlank()) {
				ztraty = ztrataDao.getFoundItems();
			} else {
				ztraty = ztrataDao.getFoundItemsByName(name);
			}
		}
		// Calculates number of pages.
		final int numberOfPages = (int) ((double) ztraty.size() / 5 + 0.9);
		// Creates an array of pages.
		final int[] pages;
		if (numberOfPages > 0) {
			pages = new int[numberOfPages];
		} else {
			pages = new int[1];
		}
		// Fills an array of pages in order to be used by foreach jsp tag in view.
		for (int i = 0; i < pages.length; i++) {
			pages[i] = i + 1;
		}
		// Reduces the received list "ztraty" to five or less items, depending on the
		// firstItem value and the size of the "ztraty" list.
		if (ztraty.size() < firstItem + 5) {
			ztraty = ztraty.subList(firstItem, ztraty.size());
		} else {
			ztraty = ztraty.subList(firstItem, firstItem + 5);
		}
		final LossStateDao lossStateDao = new LossStateDao();
		final List<LossState> lossStates = lossStateDao.getAll(LossState.class);
		model.addObject("ztr", ztraty);
		model.addObject("lossStates", lossStates);
		model.addObject("currentPage", numberOfPage);
		model.addObject("pages", pages);
		if (lossStateId > 0 && lossStateId <= 3) {
			model.addObject("lossStateId", lossStateId);
		}
		if (name != null && !name.isBlank()) {
			model.addObject("name", name);
		}
		return model;
	}

	/**
	 * Gets all items owned by a user. Can be limited by item´s name and its
	 * lossState category. And by paging.
	 * 
	 * @param name         name of items
	 * @param lossStateId  id of a loss state category
	 * @param numberOfPage number of current page
	 * @return view with table of own items.
	 */
	@GetMapping(path = "/vlastniVeci")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public ModelAndView vlastniVeci(@RequestParam(required = false) final String name,
			@RequestParam(defaultValue = "0") final int lossStateId,
			@RequestParam(defaultValue = "0") int numberOfPage) {
		// Calculates first item depending on the number of current page.
		final int firstItem;
		if (numberOfPage < 2) {
			numberOfPage = 1;
			firstItem = 0;
		} else {
			firstItem = 5 * (numberOfPage - 1);
		}
		final ModelAndView model = new ModelAndView("vlastniVeci");
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		final ZtrataDao ztrataDao = new ZtrataDao();
		List<Ztrata> ztraty;
		if ((name != null && !name.isBlank()) && (lossStateId > 0 && lossStateId <= 3)) {
			ztraty = ztrataDao.getItemsForUserByNameAndCategory(name, lossStateId, user.getId());
		} else if (name != null && !name.isBlank()) {
			ztraty = ztrataDao.getItemsForUserByName(name, user.getId());
		} else if (lossStateId > 0 && lossStateId <= 3) {
			ztraty = ztrataDao.getItemsForUserByCategory(user.getId(), lossStateId);
		} else {
			ztraty = ztrataDao.getItemsForUser(user.getId());
		}
		final int numberOfPages = (int) ((double) ztraty.size() / 5 + 0.9);
		final int[] pages;
		if (numberOfPages > 0) {
			pages = new int[numberOfPages];
		} else {
			pages = new int[1];
		}
		for (int i = 0; i < pages.length; i++) {
			pages[i] = i + 1;
		}
		if (ztraty.size() < firstItem + 5) {
			ztraty = ztraty.subList(firstItem, ztraty.size());
		} else {
			ztraty = ztraty.subList(firstItem, firstItem + 5);
		}
		final LossStateDao lossStateDao = new LossStateDao();
		final List<LossState> lossStates = lossStateDao.getAll(LossState.class);
		model.addObject("ztr", ztraty);
		model.addObject("pages", pages);
		model.addObject("currentPage", numberOfPage);
		model.addObject("lossStates", lossStates);
		if (lossStateId > 0 && lossStateId <= 3) {
			model.addObject("lossStateId", lossStateId);
		}
		if (name != null && !name.isBlank()) {
			model.addObject("name", name);
		}
		return model;
	}

	/**
	 * Gets an item based on its id and other information around it for the
	 * detailedItemInfo view.
	 * 
	 * @param id of an item
	 * @return view with detailed information of the item.
	 */
	@GetMapping(path = "/detailedItemInfo")
	public ModelAndView detailedItemInfo(@RequestParam(defaultValue = "0") final int id) {
		if (id < 1) {
			final ModelAndView model = new ModelAndView("index");
			model.addObject("error", "Pokoušíte se zobrazit podrobné informace neexistujícího předmětu!");
			return model;
		}
		final ModelAndView model = new ModelAndView("detailedItemInfo");
		final ZtrataDao ztrataDao = new ZtrataDao();
		final Ztrata ztrata = ztrataDao.getById(id, Ztrata.class);
		model.addObject(ztrata);
		final ReportingUserDao reportingUserDao = new ReportingUserDao();
		final List<ReportingUser> reportingUsers = reportingUserDao.getByItem(ztrata.getId());
		model.addObject("reportingUsers", reportingUsers);
		boolean reported = false;
		boolean ownItem = false;
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());

		if (ztrata.getUser().getId() == user.getId()) {
			ownItem = true;
		}
		// Checks if the user reported himself as an owner of hte item.
		if (!ownItem && !reportingUsers.isEmpty()) {
			for (ReportingUser reportingUser : reportingUsers) {
				if (user.getId() == reportingUser.getUser().getId()) {
					reported = true;
					break;
				}
			}
		}
		// Formats the date of the returned item, if it exists.
		if (ztrata.getItemReturned() != null) {
			final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			final String formattedDateOfReturn = dateFormat.format(ztrata.getItemReturned().getTimeOfReturn());
			model.addObject("dateOfReturn", formattedDateOfReturn);
		}
		model.addObject("reported", reported);
		model.addObject("ownItem", ownItem);
		return model;
	}

}
