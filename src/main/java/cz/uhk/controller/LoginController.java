package cz.uhk.controller;

import javax.validation.Valid;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.dao.CityDao;
import cz.uhk.dao.RightDao;
import cz.uhk.dao.UserDao;
import cz.uhk.model.City;
import cz.uhk.model.Rights;
import cz.uhk.model.User;

/**
 * Manages login and registration.
 * 
 * @author Artur Hamza
 *
 */
@Controller
public class LoginController {
	@GetMapping(path = "/login")
	public ModelAndView index() {
		final ModelAndView model = new ModelAndView("login");
		final User user = new User();
		model.addObject(user);
		return model;
	}

	/**
	 * Sends successfully user to index..
	 * 
	 * @return redirection to index.
	 */
	@RequestMapping(path = "/postLogin", method = RequestMethod.POST)
	public String postLogin() {
		return "redirect:/";
	}

	/**
	 * In case of user failing to login redirects user back to login action.
	 * 
	 * @param redirectatt instance for saving data for a redirected action.
	 * @return redirection back to login action.
	 */
	@GetMapping(value = "/loginFailed")
	public String loginError(final RedirectAttributes redirectatt) {
		redirectatt.addFlashAttribute("error", "Chybné jméno, nebo heslo!");
		return "redirect:/login";
	}

	/**
	 * Creates model for the user´s registration.
	 * 
	 * @return view with form for registration of a user.
	 */
	@GetMapping(value = "register")
	public ModelAndView register() {
		final ModelAndView model = new ModelAndView("register");
		model.addObject(new User());
		return model;
	}

	/**
	 * Process the registration of a user.
	 * 
	 * @param user          user information from the form.
	 * @param passwordAgain password of the user written again.
	 * @param agree         refusal or confirmation of user to agree with terms of
	 *                      use of an app.
	 * @param result        result of validation of the user information.
	 * @param redirectatt   instance for saving data for a redirected action.
	 * @return result of an action.
	 */
	@RequestMapping(value = "registration", method = RequestMethod.POST)
	public ModelAndView registration(@Valid final User user, final BindingResult result,
			@RequestParam final String passwordAgain, @RequestParam final boolean agree,
			final RedirectAttributes redirectatt) {
		final ModelAndView model;
		if (result.hasErrors()) {
			model = new ModelAndView("register");
			model.addObject(user);
			return model;
		}
		if (!agree) {
			model = new ModelAndView("register");
			model.addObject(user);
			model.addObject("error", "Pro registraci musíte souhlasit s našimi licenčními podmínkami.");
			return model;
		}
		if (passwordAgain.isBlank()) {
			model = new ModelAndView("register");
			model.addObject(user);
			model.addObject("error", "Položka heslo znova musí být vyplněna!");
			return model;
		}
		if (!passwordAgain.equals(user.getPassword())) {
			model = new ModelAndView("register");
			model.addObject(user);
			model.addObject("error",
					"Zadal jste dvakrát jiné heslo. Heslo musí být do položek Heslo a Heslo Znova zadáno stejně!");
			return model;
		}
		final UserDao userDao = new UserDao();
		// Checks if the login parameter is already used by other account. If yes, the
		// user is returned to the registration page.
		if (userDao.getByLogin(user.getLogin()) != null) {
			model = new ModelAndView("register");
			model.addObject(user);
			model.addObject("error", "Přihlašovací jméno je už použito. Zadejte prosím jiné!");
			return model;
		}
		final RightDao rightDao = new RightDao();
		user.setRights(rightDao.getById(2, Rights.class));
		final CityDao cityDao = new CityDao();
		// Checks if the city already exists by its PSC, if not, the city is created.
		if (cityDao.getByPSC(user.getCity().getPsc()) != null) {
			user.setCity(cityDao.getByPSC(user.getCity().getPsc()));
		} else {
			final City city = user.getCity();
			cityDao.create(city);
		}
		final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		user.setPassword(encoder.encode(user.getPassword()));
		userDao.create(user);
		model = new ModelAndView("redirect:/login");
		redirectatt.addFlashAttribute("success", "Vytvoření účtu " + user.getLogin() + " proběhlo úspěšně.");
		return model;
	}
}
