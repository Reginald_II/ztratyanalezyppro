package cz.uhk.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cz.uhk.dao.PictureDao;
import cz.uhk.model.Picture;

/**
 * Controller used by views for getting a picture of an item from the database.
 * 
 * @author Artur Hamza
 *
 */
@Controller
public class PictureController {
	/**
	 * Gets a picture from the database and returns it in an output stream of an
	 * Http servlet response for a view.
	 * 
	 * @param response response to the request for a picture.
	 * @param id       id of a picture.
	 */
	@RequestMapping("/getPicture")
	public void getPicture(final HttpServletResponse response, @RequestParam final int id) {
		final PictureDao pictureDao = new PictureDao();
		final Picture picture = pictureDao.getById(id, Picture.class);
		// Sets the response´s type to the image´s one.
		if (picture.getType().equals("jpg")) {
			response.setContentType("image/jpeg");
		} else {
			response.setContentType("image/png");
		}
		try {
			response.getOutputStream().write(picture.getFile());
			response.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
