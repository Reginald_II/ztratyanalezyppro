package cz.uhk.controller;

import java.io.IOException;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.dao.LossStateDao;
import cz.uhk.dao.PictureDao;
import cz.uhk.dao.UserDao;
import cz.uhk.dao.ZtrataDao;
import cz.uhk.model.LossState;
import cz.uhk.model.Picture;
import cz.uhk.model.User;
import cz.uhk.model.Ztrata;

/**
 * Controller, which works with items.
 * 
 * @author Artur Hamza
 *
 */
@Controller
@Secured({ "ROLE_ADMIN", "ROLE_USER" })
public class ZtrataController {
	/**
	 * Creates model for adding of a found item form.
	 * 
	 * @return view with form for adding found items.
	 */
	@GetMapping(path = "/pridatNalez")
	@Secured("ROLE_ADMIN")
	public ModelAndView pridatNalez() {
		final ModelAndView model = new ModelAndView("pridatNalez");
		final Ztrata ztrata = new Ztrata();
		model.addObject("ztrata", ztrata);
		return model;
	}

	/**
	 * Processess adding of a found item.
	 * 
	 * @param ztrata      found item from the form.
	 * @param pictureFile multipart file from the form.
	 * @param result      result of a validation of the found item.
	 * @param redirectatt instance for saving data for redirected action.
	 * @return result of the action.
	 */
	@RequestMapping(path = "/pridaniNalezu", method = RequestMethod.POST)
	@Secured("ROLE_ADMIN")
	public ModelAndView pridaniNalezu(@Valid final Ztrata ztrata,
			@RequestParam(required = false) final MultipartFile pictureFile, final BindingResult result,
			final RedirectAttributes redirectatt) {
		if (result.hasErrors()) {
			final ModelAndView model = new ModelAndView("pridatNalez");
			model.addObject(ztrata);
			return model;
		}
		if (!pictureFile.isEmpty()) {
			final String name = pictureFile.getOriginalFilename();
			if (!name.endsWith(".jpg") && name.endsWith(".png")) {
				final ModelAndView model = new ModelAndView("pridatNalez");
				model.addObject(ztrata);
				model.addObject("error", "Fotka musí mít formát .jpg nebo .png!");
				return model;
			}
			// Gets size of the picture in bytes.
			long size = pictureFile.getSize();
			// Converts the size of the picture to kilobytes.
			size /= 1000 + 0.5;
			if (size > 16000) {
				final ModelAndView model = new ModelAndView("pridatNalez");
				model.addObject(ztrata);
				model.addObject("error", "Fotka nesmí být větší než 16 MB!");
				return model;
			}
			final short pictureSize = (short) size;
			final int typeIndex = name.lastIndexOf(".");
			final String type = name.substring(typeIndex + 1);
			try {
				final byte[] file = pictureFile.getInputStream().readAllBytes();
				final Picture picture = new Picture(name, type, pictureSize, file);
				PictureDao pictureDao = new PictureDao();
				pictureDao.create(picture);
				ztrata.setPicture(picture);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		final LossStateDao lossStateDao = new LossStateDao();
		ztrata.setLossState(lossStateDao.getById(2, LossState.class));
		ztrata.setUser(user);
		final ZtrataDao ztrataDao = new ZtrataDao();
		ztrata.setWasCreatedAsLoss(false);
		ztrata.setTimeCreated(new Date());
		ztrata.setTimeChanged(new Date());
		ztrataDao.create(ztrata);
		final ModelAndView model = new ModelAndView("redirect:/");
		redirectatt.addFlashAttribute("success", "Nález, " + ztrata.getNazev() + ", byl úspěšně vytvořen.");
		return model;
	}

	/**
	 * Creates model for creating of lost item form.
	 * 
	 * @return view with form for creating lost item.
	 */
	@GetMapping(path = "/pridatZtratu")
	public ModelAndView pridatZtratu() {
		final ModelAndView model = new ModelAndView("pridatZtratu");
		final Ztrata ztrata = new Ztrata();
		model.addObject("ztrata", ztrata);
		return model;
	}

	/**
	 * Manages adding lost item to the database.
	 * 
	 * @param ztrata      lost item from the form.
	 * @param result      result of the validation of the found item.
	 * @param pictureFile multi part file from the form.
	 * @param redirectatt instance to save data for redirected action.
	 * @return result of the action.
	 */
	@RequestMapping(path = "/pridaniZtraty", method = RequestMethod.POST)
	public ModelAndView pridaniZtraty(@Valid final Ztrata ztrata, final BindingResult result,
			@RequestParam(required = false) final MultipartFile pictureFile, final RedirectAttributes redirectatt) {
		if (result.hasErrors()) {
			final ModelAndView model = new ModelAndView("pridatZtratu");
			model.addObject(ztrata);
			return model;
		}
		if (!pictureFile.isEmpty()) {
			final String name = pictureFile.getOriginalFilename();
			if (!name.endsWith(".jpg") && name.endsWith(".png")) {
				final ModelAndView model = new ModelAndView("pridatZtratu");
				model.addObject(ztrata);
				model.addObject("error", "Fotka musí mít formát .jpg nebo .png!");
				return model;
			}
			// Gets size of the picture in bytes.
			long size = pictureFile.getSize();
			// Converts the size of the picture to kilobytes.
			size /= 1000 + 0.5;
			if (size > 16000) {
				final ModelAndView model = new ModelAndView("pridatZtratu");
				model.addObject(ztrata);
				model.addObject("error", "Fotka nesmí být větší než 16 MB!");
				return model;
			}
			final short pictureSize = (short) size;
			final int typeIndex = name.lastIndexOf(".");
			final String type = name.substring(typeIndex + 1);
			try {
				final byte[] file = pictureFile.getInputStream().readAllBytes();
				final Picture picture = new Picture(name, type, pictureSize, file);
				PictureDao pictureDao = new PictureDao();
				pictureDao.create(picture);
				ztrata.setPicture(picture);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		final LossStateDao lossStateDao = new LossStateDao();
		ztrata.setLossState(lossStateDao.getById(1, LossState.class));
		ztrata.setUser(user);
		final ZtrataDao ztrataDao = new ZtrataDao();
		ztrata.setWasCreatedAsLoss(true);
		ztrata.setTimeCreated(new Date());
		ztrata.setTimeChanged(new Date());
		ztrataDao.create(ztrata);
		final ModelAndView model = new ModelAndView("redirect:/vlastniVeci");
		redirectatt.addFlashAttribute("success", "Ztráta, " + ztrata.getNazev() + ", byla úspěšně vytvořena.");
		return model;
	}

	/**
	 * Creates model for changing of item.
	 * 
	 * @param id id of an item.
	 * @return view with form for changing the item.
	 */
	@GetMapping(path = "/upravitPredmet")
	@Secured("ROLE_ADMIN")
	public ModelAndView upravitPredmet(@RequestParam final int id) {
		final ZtrataDao ztrataDao = new ZtrataDao();
		final Ztrata ztrata = ztrataDao.getById(id, Ztrata.class);
		final ModelAndView model = new ModelAndView("upravitPredmet");
		model.addObject(ztrata);
		return model;
	}

	/**
	 * Manages the saving of a changed item in the database.
	 * 
	 * @param ztrata      item from the form.
	 * @param result      result of the validation of hte item.
	 * @param pictureFile multipart file from the form.
	 * @param redirectatt instance to save data for redirected action.
	 * @return result of the action
	 */
	@RequestMapping(path = "/upraveniPredmetu", method = RequestMethod.POST)
	@Secured("ROLE_ADMIN")
	public ModelAndView upraveniPredmetu(@Valid final Ztrata ztrata, final BindingResult result,
			@RequestParam(required = false) final MultipartFile pictureFile, final RedirectAttributes redirectatt) {
		final ModelAndView model;
		if (result.hasErrors()) {
			model = new ModelAndView("upravitPredmet");
			model.addObject(ztrata);
			return model;
		}
		final ZtrataDao ztrataDao = new ZtrataDao();
		// Gets original item to do required checks.
		final Ztrata validateZtrata = ztrataDao.getById(ztrata.getId(), Ztrata.class);
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		// Checks if the user has user role and if yes, if he is trying to change other
		// user´s item.
		if (user.getRights().getName().equals("ROLE_USER")) {
			if (user.getId() != ztrata.getUser().getId() || user.getId() != validateZtrata.getUser().getId()) {
				model = new ModelAndView("redirect:/");
				redirectatt.addFlashAttribute("error", "Snažíte se upravit cizí předmět!");
				return model;
			}
		}
		if (validateZtrata.getPicture() != null && pictureFile.isEmpty()) {
			ztrata.setPicture(validateZtrata.getPicture());
		}
		if (!pictureFile.isEmpty()) {
			final String name = pictureFile.getOriginalFilename();
			if (!name.endsWith(".jpg") && name.endsWith(".png")) {
				model = new ModelAndView("upravitPredmet");
				model.addObject(ztrata);
				model.addObject("error", "Fotka musí mít formát .jpg nebo .png!");
				return model;
			}
			// Gets size of the picture in bytes.
			long size = pictureFile.getSize();
			// Converts the size of the picture to kilobytes.
			size /= 1000 + 0.5;
			if (size > 16000) {
				model = new ModelAndView("upravitPredmet");
				model.addObject(ztrata);
				model.addObject("error", "Fotka nesmí být větší než 16 MB!");
				return model;
			}
			final short pictureSize = (short) size;
			final int typeIndex = name.lastIndexOf(".");
			final String type = name.substring(typeIndex + 1);
			try {
				final byte[] file = pictureFile.getInputStream().readAllBytes();
				final Picture picture = new Picture(name, type, pictureSize, file);
				PictureDao pictureDao = new PictureDao();
				// Checks if the loss has already picture attached. If yes, the picture is
				// overwritten. Otherwise, the picture is made as new row.
				if (validateZtrata.getPicture() != null) {
					picture.setId(validateZtrata.getPicture().getId());
					pictureDao.merge(picture);
				} else {
					pictureDao.create(picture);
				}
				ztrata.setPicture(picture);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		ztrata.setLossState(validateZtrata.getLossState());
		ztrata.setUser(validateZtrata.getUser());
		ztrata.setWasCreatedAsLoss(validateZtrata.isWasCreatedAsLoss());
		ztrata.setTimeCreated(validateZtrata.getTimeCreated());
		ztrata.setTimeChanged(new Date());
		ztrataDao.merge(ztrata);
		model = new ModelAndView("redirect:/");
		redirectatt.addFlashAttribute("success", "Předmět, " + ztrata.getNazev() + ", byl úspěšně upraven.");
		return model;
	}

	/**
	 * Creates a model for form of changing own item.
	 * 
	 * @param id          id of an item.
	 * @param redirectatt instance for saving data for redirected action.
	 * @return view with form to change own item.
	 */
	@GetMapping(path = "/upravitVlastniPredmet")
	public ModelAndView upravitVlastniPredmet(@RequestParam final int id, final RedirectAttributes redirectatt) {
		final ZtrataDao ztrataDao = new ZtrataDao();
		final Ztrata ztrata = ztrataDao.getById(id, Ztrata.class);
		final ModelAndView model;
		// Checks if an user is trying to change returned item, which can´t be changed.
		if (ztrata.getLossState().getId() == 3) {
			model = new ModelAndView("redirect:/vlastniVeci");
			redirectatt.addFlashAttribute("error", "Nelze upravit vrácený předmět!");
			return model;
		}
		model = new ModelAndView("upravitVlastniPredmet");
		model.addObject(ztrata);
		return model;
	}

	/**
	 * Processes changing of own item.
	 * 
	 * @param ztrata      lost item from the form.
	 * @param result      result of validation of the item.
	 * @param pictureFile multipart file from the form.
	 * @param redirectatt instance for saving data for redirected action.
	 * @return result of the action.
	 */
	@RequestMapping(path = "/upraveniVlastnihoPredmetu", method = RequestMethod.POST)
	public ModelAndView upraveniVlastnihoPredmetu(@Valid final Ztrata ztrata, final BindingResult result,
			@RequestParam(required = false) final MultipartFile pictureFile, final RedirectAttributes redirectatt) {
		final ModelAndView model;
		if (result.hasErrors()) {
			model = new ModelAndView("upravitVlastniPredmet");
			model.addObject(ztrata);
			return model;
		}
		final ZtrataDao ztrataDao = new ZtrataDao();
		// Gets original item for required checks.
		final Ztrata validateZtrata = ztrataDao.getById(ztrata.getId(), Ztrata.class);
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		// Checks if user is trying to change the item which doesn´t belong to him.
		if (user.getId() != validateZtrata.getUser().getId()) {
			model = new ModelAndView("redirect:/vlastniVeci");
			redirectatt.addFlashAttribute("error", "Snažíte se upravit cizí ztrátu!");
			return model;
		}
		// Checks if user is trying to change returned item, which can´t be changed.
		if (validateZtrata.getLossState().getId() == 3) {
			model = new ModelAndView("redirect:/vlastniVeci");
			redirectatt.addFlashAttribute("error", "Nelze upravit vrácený předmět!");
			return model;
		}
		if (validateZtrata.getPicture() != null && pictureFile.isEmpty()) {
			ztrata.setPicture(validateZtrata.getPicture());
		}
		if (!pictureFile.isEmpty()) {
			final String name = pictureFile.getOriginalFilename();
			if (!name.endsWith(".jpg") && name.endsWith(".png")) {
				model = new ModelAndView("upravitVlastniPredmet");
				model.addObject(ztrata);
				model.addObject("error", "Fotka musí mít formát .jpg nebo .png!");
				return model;
			}
			// Gets size of the picture in bytes.
			long size = pictureFile.getSize();
			// Converts the size of the picture to kilobytes.
			size /= 1000 + 0.5;
			if (size > 16000) {
				model = new ModelAndView("upravitVlastniPredmet");
				model.addObject(ztrata);
				model.addObject("error", "Fotka nesmí být větší než 16 MB!");
				return model;
			}
			final short pictureSize = (short) size;
			final int typeIndex = name.lastIndexOf(".");
			final String type = name.substring(typeIndex + 1);
			try {
				final byte[] file = pictureFile.getInputStream().readAllBytes();
				final Picture picture = new Picture(name, type, pictureSize, file);
				PictureDao pictureDao = new PictureDao();
				// Checks if the loss has already picture attached. If yes, the picture is
				// overwritten. Otherwise, the picture is made as new row.
				if (validateZtrata.getPicture() != null) {
					picture.setId(validateZtrata.getPicture().getId());
					pictureDao.merge(picture);
				} else {
					pictureDao.create(picture);
				}
				ztrata.setPicture(picture);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		ztrata.setLossState(validateZtrata.getLossState());
		ztrata.setUser(validateZtrata.getUser());
		ztrata.setWasCreatedAsLoss(validateZtrata.isWasCreatedAsLoss());
		ztrata.setTimeCreated(validateZtrata.getTimeCreated());
		ztrata.setTimeChanged(new Date());
		ztrataDao.merge(ztrata);
		model = new ModelAndView("redirect:/vlastniVeci");
		redirectatt.addFlashAttribute("success", "Předmět, " + ztrata.getNazev() + ", byl úspěšně upraven.");
		return model;
	}

	/**
	 * Processess removal of lost item from database.
	 * 
	 * @param id          id of an item to be removed.
	 * @param redirectatt instance to save data for redirected action.
	 * @return result of the action.
	 */
	@GetMapping(path = "/smazatZtratu")
	@Secured("ROLE_ADMIN")
	public String smazaniZtraty(@RequestParam final int id, final RedirectAttributes redirectatt) {
		final ZtrataDao ztrataDao = new ZtrataDao();
		final Ztrata ztrata = ztrataDao.getById(id, Ztrata.class);
		// Checks if item is lost.
		if (ztrata.getLossState().getId() != 1) {
			redirectatt.addFlashAttribute("error", "Lze smazat pouze ztráty!");
			return "redirect:/";
		}
		final String ztrataName = ztrata.getNazev();
		// Saves a picture of the lost item in memory.
		final Picture picture = ztrata.getPicture();
		ztrataDao.delete(ztrata);
		// Checks if the lost item had a picture. If yes, the picture is deleted.
		if (picture != null) {
			final PictureDao pictureDao = new PictureDao();
			pictureDao.delete(picture);
		}
		redirectatt.addFlashAttribute("success", "Ztráta, " + ztrataName + ", byla úspěšně smazána");
		return "redirect:/";
	}

	/**
	 * 
	 * @param id          of an item to be removed
	 * @param redirectatt instance to save data for redirected action.
	 * @return result of the action.
	 */
	@GetMapping(path = "/smazatVlastniZtratu")
	public String smazaniVlastniZtraty(@RequestParam final int id, final RedirectAttributes redirectatt) {
		final ZtrataDao ztrataDao = new ZtrataDao();
		final Ztrata ztrata = ztrataDao.getById(id, Ztrata.class);
		final UserDao userDao = new UserDao();
		final User user = userDao.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		// Checks if user is trying to delete lost item which doesn´t belong to him.
		if (user.getId() != ztrata.getUser().getId()) {
			redirectatt.addFlashAttribute("error", "Snažíte se smazat cizí ztrátu!");
			return "redirect:/vlastniVeci";
		}
		// Checks if user is trying to delete found or returned item.
		if (ztrata.getLossState().getId() != 1) {
			redirectatt.addFlashAttribute("error", "Lze smazat pouze ztrátu!");
			return "redirect:/vlastniVeci";
		}
		final String ztrataName = ztrata.getNazev();
		final Picture picture = ztrata.getPicture();
		ztrataDao.delete(ztrata);
		if (picture != null) {
			final PictureDao pictureDao = new PictureDao();
			pictureDao.delete(picture);
		}
		redirectatt.addFlashAttribute("success", "Ztráta, " + ztrataName + ", byla úspěšně smazána");
		return "redirect:/vlastniVeci";
	}

	/**
	 * Processess changing the loss state category of lost item to found one.
	 * 
	 * @param id          id of a lost item.
	 * @param redirectatt instance to save data for redirected action.
	 * @return result of the action.
	 */
	@GetMapping(path = "/prevestZtratuNaNalez")
	@Secured("ROLE_ADMIN")
	public String prevestZtratuNaNalez(@RequestParam final int id, final RedirectAttributes redirectatt) {
		final ZtrataDao ztrataDao = new ZtrataDao();
		final Ztrata ztrata = ztrataDao.getById(id, Ztrata.class);
		// Checks if the item doesn´t have other category than loss.
		if (ztrata.getLossState().getId() != 1) {
			redirectatt.addFlashAttribute("error", "Lze převést na nález pouze ztrátu!");
			return "redirect:/";
		}
		final LossStateDao lossStateDao = new LossStateDao();
		ztrata.setLossState(lossStateDao.getById(2, LossState.class));
		ztrataDao.update(ztrata);
		redirectatt.addFlashAttribute("success", "Ztráta " + ztrata.getNazev() + " byla úspěšně převedena na nález.");
		return "redirect:/";
	}
}