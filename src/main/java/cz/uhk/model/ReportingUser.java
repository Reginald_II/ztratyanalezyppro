package cz.uhk.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "reporting_user")
/**
 * Model class, which works with information of user reporting himself for an
 * ownership of an item.
 * 
 * @author Artur Hamza
 *
 */
public class ReportingUser {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private int id;
	@Column(name = "time_reported", nullable = false)
	private Date timeReported;
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;
	@ManyToOne
	@JoinColumn(name = "ztrata_id", nullable = false)
	private Ztrata ztrata;

	public ReportingUser() {

	}

	public ReportingUser(final Date timeReported, final User user, final Ztrata ztrata) {
		this.timeReported = timeReported;
		this.user = user;
		this.ztrata = ztrata;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getTimeReported() {
		return timeReported;
	}

	public void setTimeReported(Date timeReported) {
		this.timeReported = timeReported;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Ztrata getZtrata() {
		return ztrata;
	}

	public void setZtrata(Ztrata ztrata) {
		this.ztrata = ztrata;
	}

}
