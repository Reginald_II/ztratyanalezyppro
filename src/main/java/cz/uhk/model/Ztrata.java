package cz.uhk.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

/**
 * Model class of the lost, found or gave item.
 * 
 * @author Artur Hamza
 *
 */
@Entity
@Table(name = "ztrata")
public class Ztrata {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private int id;

	@Column(name = "nazev", length = 100, nullable = false)
	@NotBlank(message = "Prosím vyplňte název!")
	@Size(min = 1, max = 100, message = "Prosím vložte text o max. velikosti 100 znaků!")
	private String nazev;

	@Column(name = "popis", length = 1000, nullable = false)
	@Size(max = 100, message = "Prosím vložte popis o max. velikosti 1000 znaků!")
	private String popis;

	@Column(name = "timeCreated", nullable = false)
	private Date timeCreated;

	@Column(name = "timeChanged", nullable = false)
	private Date timeChanged;

	@Column(name = "wasCreatedAsLoss", nullable = false)
	private boolean wasCreatedAsLoss;

	@ManyToOne
	@JoinColumn(name = "loss_state_id", nullable = false)
	private LossState lossState;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@ManyToOne
	@JoinColumn(name = "returned_id", nullable = true)
	private ItemReturned itemReturned;
	@ManyToOne
	@JoinColumn(name = "picture_id", nullable = true)
	private Picture picture;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNazev() {
		return nazev;
	}

	public void setNazev(String nazev) {
		this.nazev = nazev;
	}

	public String getPopis() {
		return popis;
	}

	public void setPopis(String popis) {
		this.popis = popis;
	}

	public LossState getLossState() {
		return lossState;
	}

	public void setLossState(LossState lossState) {
		this.lossState = lossState;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeChanged() {
		return timeChanged;
	}

	public void setTimeChanged(Date timeChanged) {
		this.timeChanged = timeChanged;
	}

	public ItemReturned getItemReturned() {
		return itemReturned;
	}

	public void setItemReturned(ItemReturned itemReturned) {
		this.itemReturned = itemReturned;
	}

	public boolean isWasCreatedAsLoss() {
		return wasCreatedAsLoss;
	}

	public void setWasCreatedAsLoss(boolean wasCreatedAsLoss) {
		this.wasCreatedAsLoss = wasCreatedAsLoss;
	}

	public Picture getPicture() {
		return picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}

}
