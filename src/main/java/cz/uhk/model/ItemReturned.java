package cz.uhk.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Model class of the information gained when an item is returned to a user.
 * 
 * @author Artur Hamza
 *
 */
@Entity
@Table(name = "item_returned")
public class ItemReturned {
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private int id;
	@Column(name = "timeOfReturn", nullable = false)
	private Date timeOfReturn;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	public ItemReturned() {
	}

	public ItemReturned(final Date timeOfReturn, final User user) {
		this.timeOfReturn = timeOfReturn;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getTimeOfReturn() {
		return timeOfReturn;
	}

	public void setTimeOfReturn(Date timeOfReturn) {
		this.timeOfReturn = timeOfReturn;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
