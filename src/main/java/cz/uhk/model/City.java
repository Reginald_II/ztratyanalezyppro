package cz.uhk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Model class, which contains information of cities of users.
 * 
 * @author Artur Hamza
 *
 */
@Entity
@Table(name = "city")
public class City {
	@Id
	@Column(name = "psc", length = 5, nullable = false)
	@Min(value = 1, message = "Prosím vyplňte PSČ! Pokud jste ho zadal s mezerami, zadejte ho prosím znova bez mezer!")
	@Digits(integer = 5, fraction = 0, message = "PSČ může mít maximálně 5 číslic! Zadejte ho prosím znova.")
	private int psc;
	@Column(name = "name", length = 100, nullable = false)
	@NotBlank(message = "Vyplňte název města!")
	@Size(max = 100, message = "Název města nesmí přesahovat 100 znaků!")
	private String name;

	public City() {
	}

	public City(final int psc, final String name) {
		this.psc = psc;
		this.name = name;
	}

	public int getPsc() {
		return psc;
	}

	public void setPsc(int psc) {
		this.psc = psc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
