package cz.uhk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

/**
 * Model class, where are saved data of the stored picture of an item.
 * 
 * @author Artur Hamza
 *
 */
@Entity
@Table(name = "picture")
public class Picture {
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private int id;

	@Column(name = "name", length = 50, nullable = false)
	@NotBlank(message = "Prosím vyplňte název!")
	@Size(min = 1, max = 100, message = "Prosím vložte text o max. velikosti 50 znaků!")
	private String name;

	@Column(name = "type", length = 50, nullable = false)
	@NotBlank(message = "Prosím vyplňte typ souboru!")
	@Size(min = 1, max = 100, message = "Prosím vložte text o max. velikosti 10 znaků!")
	private String type;

	@Column(name = "size", nullable = false)
	@Min(value = 1, message = "Velikost souboru musí být vyplněna!")
	@Max(value = 16000, message = "Obrázek nesmí být větší než 16 000 KB!")
	private short size;

	@Column(name = "file", nullable = false)
	private byte[] file;

	public Picture() {
	}

	public Picture(final String name, final String type, final short size, final byte[] file) {
		this.name = name;
		this.type = type;
		this.size = size;
		this.file = file;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public short getSize() {
		return size;
	}

	public void setSize(short size) {
		this.size = size;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

}
