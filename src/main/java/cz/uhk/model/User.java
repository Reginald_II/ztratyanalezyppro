package cz.uhk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

/**
 * Model class of the user.
 * 
 * @author Artur Hamza
 *
 */
@Entity
@Table(name = "user")
public class User {
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private int id;
	@Column(name = "login", length = 50, nullable = false)
	@NotBlank(message = "Vyplňte přihlašovací jméno!")
	@Size(max = 50, message = "Přihlašovací jméno nesmí mít víc jak 50 znaků!")
	private String login;
	@Column(name = "password", length = 100, nullable = false)
	@NotBlank(message = "Vyplňte heslo!")
	@Size(max = 100, message = "Heslo nesmí mít víc jak 100 znaků!")
	private String password;
	@Column(name = "name", length = 50, nullable = false)
	@NotBlank(message = "Vyplňte vaše jméno!")
	@Size(max = 50, message = "Jméno nesmí mít víc jak 50 znaků!")
	private String name;
	@Column(name = "surname", length = 50, nullable = false)
	@NotBlank(message = "Vyplňte vaše přijmení!")
	@Size(max = 50, message = "Přijmení nesmí mít víc jak 50 znaků!")
	private String surname;
	@Column(name = "email", length = 100, nullable = false)
	@NotBlank(message = "Vyplňte váš E-mail!")
	@Size(max = 100, message = "E-mail nesmí mít víc jak 100 znaků!")
	private String email;
	@Column(name = "telephone", nullable = false)
	@Min(value = 1, message = "Vyplňte Vaše telefonní číslo! Pokud jste ho zadali s mezerami, vyplňte ho znova beze mezer!")
	@Digits(integer = 9, fraction = 0, message = "Telefonní číslo může mít maximálně 9 číslic! Zadejte ho prosím znova.")
	private int telephone;
	@Column(name = "streetName", length = 100, nullable = false)
	@NotBlank(message = "Vyplňte název ulice vašeho bydliště!")
	@Size(max = 100, message = "Název ulice nesmí mít víc jak 100 znaků!")
	private String streetName;
	@ManyToOne
	@JoinColumn(name = "rights_id", nullable = false)
	private Rights rights;
	@ManyToOne
	@JoinColumn(name = "city_psc", nullable = false)
	@Valid
	private City city;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public Rights getRights() {
		return rights;
	}

	public void setRights(Rights rights) {
		this.rights = rights;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

}
