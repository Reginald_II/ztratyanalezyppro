package cz.uhk.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;

import cz.uhk.model.LossState;

/**
 * Dao class of the module class LossState.
 * 
 * @author Artur Hamza
 *
 */
public class LossStateDao extends DaoBase<LossState> {
	/**
	 * Gets the first two loss states from the database.
	 * 
	 * @return list of loss states.
	 */
	public List<LossState> getWithoutGiven() {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<LossState> criteriaQuery = builder.createQuery(LossState.class);
		final Root<LossState> root = criteriaQuery.from(LossState.class);
		criteriaQuery = criteriaQuery.select(root);
		final Query<LossState> query = session.createQuery(criteriaQuery);
		return query.getResultList().subList(0, 2);
	}
}
