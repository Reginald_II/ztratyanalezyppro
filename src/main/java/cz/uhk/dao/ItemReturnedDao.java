package cz.uhk.dao;

import cz.uhk.model.ItemReturned;

/**
 * Dao class of the module class ItemReturned.
 * 
 * @author Artur Hamza
 *
 */
public class ItemReturnedDao extends DaoBase<ItemReturned> {

}
