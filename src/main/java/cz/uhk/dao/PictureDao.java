package cz.uhk.dao;

import cz.uhk.model.Picture;

/**
 * Dao class of the model class Picture.
 * 
 * @author Artur Hamza
 *
 */
public class PictureDao extends DaoBase<Picture> {

}
