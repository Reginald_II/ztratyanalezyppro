package cz.uhk.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;

import cz.uhk.model.User;

/**
 * Dao class for model class User.
 * 
 * @author Artur Hamza
 *
 */
public class UserDao extends DaoBase<User> {
	/**
	 * Gets a user by his login.
	 * 
	 * @param login login of a user
	 * @return instance of User.
	 */
	public User getByLogin(final String login) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = session.getCriteriaBuilder().createQuery(User.class);
		final Root<User> root = criteriaQuery.from(User.class);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(root.get("login"), login));
		final Query<User> query = session.createQuery(criteriaQuery);
		return query.uniqueResult();
	}
}
