package cz.uhk.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;

import cz.uhk.model.City;

/**
 * Dao class of the model class City.
 * 
 * @author Artur Hamza
 *
 */
public class CityDao extends DaoBase<City> {
	/**
	 * Gets an instance from the database by a city´s psc.
	 * 
	 * @param psc post routing number of a city.
	 * @return instance of city.
	 */
	public City getByPSC(final int psc) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<City> criteriaQuery = session.getCriteriaBuilder().createQuery(City.class);
		final Root<City> root = criteriaQuery.from(City.class);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(root.get("psc"), psc));
		final Query<City> query = session.createQuery(criteriaQuery);
		return query.uniqueResult();
	}
}
