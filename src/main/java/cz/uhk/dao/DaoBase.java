package cz.uhk.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import cz.uhk.utility.HibernateUtility;

public abstract class DaoBase<T> implements IDaoBase<T> {

	protected static SessionFactory sessionFactory;
	protected static Session session;

	public DaoBase() {
		if (session == null || !session.isOpen()) {
			sessionFactory = HibernateUtility.createSessionFactory();
			session = sessionFactory.openSession();
		}
	}

	@Override
	public void create(final T t) {
		final Transaction transaction = session.beginTransaction();
		try {
			final Object o = session.save(t);
			transaction.commit();
		} catch (final Exception e) {
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}

	@Override
	public List<T> getAll(final Class<T> clas) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = builder.createQuery(clas);
		final Root<T> root = criteriaQuery.from(clas);
		criteriaQuery = criteriaQuery.select(root);
		final Query<T> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	@Override
	public T getById(final int id, final Class<T> clas) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = session.getCriteriaBuilder().createQuery(clas);
		final Root<T> root = criteriaQuery.from(clas);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(root.get("id"), id));
		final Query<T> query = session.createQuery(criteriaQuery);
		return query.uniqueResult();
	}

	@Override
	public void update(final T t) {
		final Transaction transaction = session.beginTransaction();
		try {
			session.update(t);
			transaction.commit();
		} catch (final Exception e) {
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}

	@Override
	public void delete(final T t) {
		final Transaction transaction = session.beginTransaction();
		try {
			session.delete(t);
			transaction.commit();
		} catch (final Exception e) {
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void merge(final T t) {
		final Transaction transaction = session.beginTransaction();
		try {
			session.merge(t);
			transaction.commit();
		} catch (final Exception e) {
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}

	}

}
