package cz.uhk.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;

import cz.uhk.model.LossState;
import cz.uhk.model.ReportingUser;
import cz.uhk.model.User;
import cz.uhk.model.Ztrata;

/**
 * Dao class for ReportingUsers model one.
 * 
 * @author Artur Hamza
 *
 */
public class ReportingUserDao extends DaoBase<ReportingUser> {
	/**
	 * Gets all instances of ReportingUser of a user, designed in parameter userId
	 * and for found items only.
	 * 
	 * @param userId id of a user.
	 * @return list of ReportingUser.
	 */
	public List<ReportingUser> getFoundItemsOfReportedUser(final int userId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<ReportingUser> criteriaQuery = session.getCriteriaBuilder().createQuery(ReportingUser.class);
		final Root<ReportingUser> root = criteriaQuery.from(ReportingUser.class);
		final Join<ReportingUser, User> user = root.join("user", JoinType.INNER);
		final Join<ReportingUser, Ztrata> ztrata = root.join("ztrata", JoinType.INNER);
		final Join<Ztrata, LossState> lossState = ztrata.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(user.get("id"), userId), builder.and(),
				builder.equal(lossState.get("id"), 2));
		final Query<ReportingUser> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets found items of a user, limited by name parameter.
	 * 
	 * @param name   name of items.
	 * @param userId id of a user.
	 * @return list of ReportingUser.
	 */
	public List<ReportingUser> getFoundItemsOfReportedUserByName(final String name, int userId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<ReportingUser> criteriaQuery = session.getCriteriaBuilder().createQuery(ReportingUser.class);
		final Root<ReportingUser> root = criteriaQuery.from(ReportingUser.class);
		final Join<ReportingUser, User> user = root.join("user", JoinType.INNER);
		final Join<ReportingUser, Ztrata> ztrata = root.join("ztrata", JoinType.INNER);
		final Join<Ztrata, LossState> lossState = ztrata.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(user.get("id"), userId), builder.and(),
				builder.equal(lossState.get("id"), 2), builder.and(),
				builder.like(ztrata.get("nazev"), "%" + name + "%"));
		final Query<ReportingUser> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets all instances of ReportingUser of found items.
	 * 
	 * @return list of ReportingUser.
	 */
	public List<ReportingUser> getAllFoundItemsOfReportedUsers() {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<ReportingUser> criteriaQuery = session.getCriteriaBuilder().createQuery(ReportingUser.class);
		final Root<ReportingUser> root = criteriaQuery.from(ReportingUser.class);
		final Join<ReportingUser, Ztrata> ztrata = root.join("ztrata", JoinType.INNER);
		final Join<Ztrata, LossState> lossState = ztrata.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(lossState.get("id"), 2));
		final Query<ReportingUser> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets all instances of ReportingUser of found items, limited by parameter
	 * name.
	 * 
	 * @param name name of items.
	 * @return list of ReportingUser.
	 */
	public List<ReportingUser> getAllFoundItemsOfReportedUsersByName(final String name) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<ReportingUser> criteriaQuery = session.getCriteriaBuilder().createQuery(ReportingUser.class);
		final Root<ReportingUser> root = criteriaQuery.from(ReportingUser.class);
		final Join<ReportingUser, Ztrata> ztrata = root.join("ztrata", JoinType.INNER);
		final Join<Ztrata, LossState> lossState = ztrata.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(lossState.get("id"), 2), builder.and(),
				builder.like(ztrata.get("nazev"), "%" + name + "%"));
		final Query<ReportingUser> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets all instances of ReportingUser, connected to an item. Item is searched
	 * by its id.
	 * 
	 * @param itemId id of an item.
	 * @return list of ReportingUser.
	 */
	public List<ReportingUser> getByItem(final int itemId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<ReportingUser> criteriaQuery = session.getCriteriaBuilder().createQuery(ReportingUser.class);
		final Root<ReportingUser> root = criteriaQuery.from(ReportingUser.class);
		final Join<ReportingUser, Ztrata> ztrata = root.join("ztrata", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(ztrata.get("id"), itemId));
		final Query<ReportingUser> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets an instance of ReportingUser, made by the user and for the item, both
	 * based by their id parameters.
	 * 
	 * @param itemId id of an item.
	 * @param userId id of a user.
	 * @return instance of ReportingUser.
	 */
	public ReportingUser getByItemForUser(final int itemId, final int userId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<ReportingUser> criteriaQuery = session.getCriteriaBuilder().createQuery(ReportingUser.class);
		final Root<ReportingUser> root = criteriaQuery.from(ReportingUser.class);
		final Join<ReportingUser, Ztrata> ztrata = root.join("ztrata", JoinType.INNER);
		final Join<ReportingUser, User> user = root.join("user", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(ztrata.get("id"), itemId), builder.and(),
				builder.equal(user.get("id"), userId));
		final Query<ReportingUser> query = session.createQuery(criteriaQuery);
		return query.uniqueResult();
	}
}
