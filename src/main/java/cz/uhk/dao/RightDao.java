package cz.uhk.dao;

import cz.uhk.model.Rights;

/**
 * Dao class created for the model class Rights.
 * 
 * @author Artur Hamza
 *
 */
public class RightDao extends DaoBase<Rights> {

}
