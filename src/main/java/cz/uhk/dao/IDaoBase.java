package cz.uhk.dao;

import java.util.List;

/**
 * Interface of basic methods for database.
 * 
 * @author Artur Hamza
 *
 * @param <T>
 */
public interface IDaoBase<T> {
	/**
	 * Creates a new row of a table.
	 * 
	 * @param t instance of a class.
	 */
	public void create(final T t);

	/**
	 * Gets all rows of a table..
	 * 
	 * @param clas required class
	 * @return list of all instances of a class T.
	 */
	public List<T> getAll(final Class<T> clas);

	/**
	 * Gets a single row of a table by its id.
	 * 
	 * @param id   id of a row.
	 * @param clas required class.
	 * @return instance of a class T.
	 */
	public T getById(final int id, final Class<T> clas);

	/**
	 * Updates the row.
	 * 
	 * @param t instance of a class.
	 */
	public void update(final T t);

	/**
	 * Detetes the row from the database.
	 * 
	 * @param t instance of a class.
	 */
	public void delete(final T t);

	/**
	 * Merges the instance with another instance in the Hibernate cache and then
	 * updates the row of the instance in the table. Or if there is no other
	 * instance in the cache, then it only updates the row in the table.
	 * 
	 * @param t instance of a class.
	 */
	public void merge(final T t);
}
