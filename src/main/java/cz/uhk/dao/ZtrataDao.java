package cz.uhk.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;

import cz.uhk.model.LossState;
import cz.uhk.model.User;
import cz.uhk.model.Ztrata;

/**
 * Dao class for a module class Ztrata, which works with items.
 * 
 * @author Artur Hamza
 *
 */
public class ZtrataDao extends DaoBase<Ztrata> {
	/**
	 * Gets all found items, which were created as loss.
	 * 
	 * @return list of found items, which were created as loss.
	 */
	public List<Ztrata> getFoundLossess() {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, LossState> lossState = root.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.isTrue(root.get("wasCreatedAsLoss")), builder.and(),
				builder.equal(lossState.get("id"), 2));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets found items with a name limited by parameter "name!, which were created
	 * as loss.
	 * 
	 * @param name name of items which are searched for.
	 * @return list of items, which were created as loss and have at least partially
	 *         the name same as in the param.
	 */
	public List<Ztrata> getFoundLossessByName(final String name) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, LossState> lossState = root.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.isTrue(root.get("wasCreatedAsLoss")), builder.and(),
				builder.like(root.get("nazev"), "%" + name + "%"), builder.and(),
				builder.equal(lossState.get("id"), 2));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets all found items, which weren´t created as losses.
	 * 
	 * @return list of found items, which weren´t created as losses.
	 */
	public List<Ztrata> getFoundItems() {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, LossState> lossState = root.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.isFalse(root.get("wasCreatedAsLoss")), builder.and(),
				builder.equal(lossState.get("id"), 2));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets found items of an user by his id, which were created as lost ones.
	 * 
	 * @param userId id of an user
	 * @return list of found items, which were created as lost ones and belong to a
	 *         user specified by his id.
	 */
	public List<Ztrata> getFoundLossessOfUser(final int userId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, LossState> lossState = root.join("lossState", JoinType.INNER);
		final Join<Ztrata, User> user = root.join("user", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.isTrue(root.get("wasCreatedAsLoss")), builder.and(),
				builder.equal(lossState.get("id"), 2), builder.and(), builder.equal(user.get("id"), userId));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets found items of an user by his id, which were created as lost ones.
	 * Search is limited by the name parameter.
	 * 
	 * @param name   name of items.
	 * @param userId id of an user.
	 * @return list of found items.
	 */
	public List<Ztrata> getFoundLossessOfUserByName(final String name, final int userId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, LossState> lossState = root.join("lossState", JoinType.INNER);
		final Join<Ztrata, User> user = root.join("user", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.isTrue(root.get("wasCreatedAsLoss")), builder.and(),
				builder.equal(lossState.get("id"), 2), builder.and(), builder.equal(user.get("id"), userId),
				builder.like(root.get("nazev"), "%" + name + "%"));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets all items of an user, based on his id.
	 * 
	 * @param userId id of an user.
	 * @return list of items.
	 */
	public List<Ztrata> getItemsForUser(final int userId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, User> user = root.join("user", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(user.get("id"), userId));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets all items of a lossState category of an user, based on his id and id of
	 * the lossState category.
	 * 
	 * @param userId      id of an user
	 * @param lossStateId id of a loss state category.
	 * @return list of items
	 */
	public List<Ztrata> getItemsForUserByCategory(final int userId, final int lossStateId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, User> user = root.join("user", JoinType.INNER);
		final Join<Ztrata, LossState> lossState = root.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(user.get("id"), userId), builder.and(),
				builder.equal(lossState.get("id"), lossStateId));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets all items of a lossState category of an user, based on his id, id of the
	 * lossState category and on the name parameter.
	 * 
	 * @param name        of items.
	 * @param lossStateId id of a loss state category.
	 * @param userId      id of an user
	 * @return list of items
	 */
	public List<Ztrata> getItemsForUserByNameAndCategory(final String name, final int lossStateId, final int userId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, User> user = root.join("user", JoinType.INNER);
		final Join<Ztrata, LossState> lossState = root.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.like(root.get("nazev"), "%" + name + "%"),
				builder.and(), builder.equal(lossState.get("id"), lossStateId), builder.and(),
				builder.equal(user.get("id"), userId));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets items of an user, limited by name parameter.
	 * 
	 * @param name   name of items.
	 * @param userId id of an user.
	 * @return list of items.
	 */
	public List<Ztrata> getItemsForUserByName(final String name, final int userId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, User> user = root.join("user", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.like(root.get("nazev"), "%" + name + "%"),
				builder.and(), builder.equal(user.get("id"), userId));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets all items, which have similar or same name as the name parameter.
	 * 
	 * @param name name of items.
	 * @return list of items.
	 */
	public List<Ztrata> getByName(final String name) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		criteriaQuery = criteriaQuery.select(root).where(builder.like(root.get("nazev"), "%" + name + "%"));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets all found items, which weren´t created as lost ones, which have similar
	 * or same name as the name parameter.
	 * 
	 * @param name of the items.
	 * @return list of found items.
	 */
	public List<Ztrata> getFoundItemsByName(final String name) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, LossState> lossState = root.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.isFalse(root.get("wasCreatedAsLoss")), builder.and(),
				builder.like(root.get("nazev"), "%" + name + "%"), builder.and(),
				builder.equal(lossState.get("id"), 2));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets all items of a lossState category, defined by its id.
	 * 
	 * @param lossStateId id of a loss state category.
	 * @return list of items.
	 */
	public List<Ztrata> getItemsByCategory(final int lossStateId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, LossState> lossState = root.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(lossState.get("id"), lossStateId));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	/**
	 * Gets items of a lossState category, defined by its id and by the name
	 * parameter, which limits the items by their name.
	 * 
	 * @param name        of items.
	 * @param lossStateId id of a loss state category.
	 * @return list of items.
	 */
	public List<Ztrata> getItemsByCategoryAndName(final String name, final int lossStateId) {
		final CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Ztrata> criteriaQuery = session.getCriteriaBuilder().createQuery(Ztrata.class);
		final Root<Ztrata> root = criteriaQuery.from(Ztrata.class);
		final Join<Ztrata, LossState> lossState = root.join("lossState", JoinType.INNER);
		criteriaQuery = criteriaQuery.select(root).where(builder.equal(lossState.get("id"), lossStateId), builder.and(),
				builder.like(root.get("nazev"), "%" + name + "%"));
		final Query<Ztrata> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}
}
