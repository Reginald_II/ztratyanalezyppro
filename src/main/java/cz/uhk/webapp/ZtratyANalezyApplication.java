package cz.uhk.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cz.uhk.config")
@ComponentScan("cz.uhk.controller")
@SpringBootApplication
public class ZtratyANalezyApplication {

	public static void main(final String[] args) {
		SpringApplication.run(ZtratyANalezyApplication.class, args);
	}
}
