package cz.uhk.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Test;

import cz.uhk.dao.ZtrataDao;
import cz.uhk.model.User;
import cz.uhk.model.Ztrata;
import cz.uhk.utility.HibernateUtility;

public class ZtratyANalezyApplicationTests {

	/**
	 * Tests if the app can connect to the database.
	 */
	@Test()
	public void testDatabaseConnection() {
		final SessionFactory sessionFactory = HibernateUtility.createSessionFactory();
		final Session session = sessionFactory.openSession();
		assertTrue(session.isConnected(), "Connection failed.");
		assertTrue(session.isOpen(), "Session isn´t open.");
		session.close();
		sessionFactory.close();
		System.out.println("Session was successfully established");
	}

	/**
	 * Tests if the app can retrieve data from the database.
	 */
	@Test()
	public void testDatabaseList() {
		final SessionFactory sessionFactory = HibernateUtility.createSessionFactory();
		final Session session = sessionFactory.openSession();
		assertTrue(session.isConnected(), "Connection failed.");
		assertTrue(session.isOpen(), "Session isn´t open.");
		final ZtrataDao ztrataDao = new ZtrataDao();
		final List<Ztrata> ztraty = ztrataDao.getAll(Ztrata.class);
		if (ztraty.size() <= 0) {
			fail("Ztraty list is either empty or failed to retrieve.");
		}
		final User user = ztraty.get(0).getUser();

		if (user.getName() == null || user.getName().isBlank()) {
			fail("Failed to retrieve user´s name through the connection from the Ztrata entity.");
		}
		if (user.getSurname() == null || user.getSurname().isBlank()) {
			fail("Failed to retrieve user´s surname through the connection from the Ztrata entity.");
		}
		session.close();
		sessionFactory.close();
	}

	/**
	 * Tests if the app can retrieve some lost item by its name.
	 */
	@Test()
	public void testDatabaseName() {
		final SessionFactory sessionFactory = HibernateUtility.createSessionFactory();
		final Session session = sessionFactory.openSession();
		assertTrue(session.isConnected(), "Connection failed.");
		assertTrue(session.isOpen(), "Session isn´t open.");
		final ZtrataDao ztrataDao = new ZtrataDao();
		final List<Ztrata> ztraty = ztrataDao.getByName("mobil");
		System.out.println("Number of items with name mobil.: " + ztraty.size());
		if (ztraty.size() <= 0) {
			fail("There are no lost items with name mobil.");
		}
		session.close();
		sessionFactory.close();
	}

	/**
	 * Tests if the app can retrieve an item by its type.
	 */
	@Test()
	public void testDatabaseItemSort() {
		final SessionFactory sessionFactory = HibernateUtility.createSessionFactory();
		final Session session = sessionFactory.openSession();
		assertTrue(session.isConnected(), "Connection failed.");
		assertTrue(session.isOpen(), "Session isn´t open.");
		final ZtrataDao ztrataDao = new ZtrataDao();
		// Looks for found items returned to a user.
		final List<Ztrata> ztraty = ztrataDao.getItemsByCategory(3);
		System.out.println("Number of items with name mobil.: " + ztraty.size());
		if (ztraty.size() <= 0) {
			fail("There are no returned found items.");
		}
		session.close();
		sessionFactory.close();
	}
}
